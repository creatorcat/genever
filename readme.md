# GeneVer - Generic Software Version Creation

Define the version of your software once,
generate for all common version schemes.

## Goals

Define a version using common patterns:
* `major.minor.patch` as in [SemVer](https://semver.org/)
* development phases like alpha, beta...

Ability to apply custom patterns:
* Arbitrary base version components (`1.2.3.4.5.6.7`)
* Qualifiers with build ids, revision numbers or anything

Generate a respective version string that is aligned to one of the most common version schemes:
* Gradle, Maven, Eclipse or MS-Net  

## User Guide

Latest release: `0.3`

For release notes see the [changelog](doc/changelog.md).

Continue to [userguide](doc/userguide.md)...

## Versioning Scheme

The version numbers used for the [releases](doc/changelog.md) and respective artifacts
follow the general [SemVer](https://semver.org/) approach in their base components (`major.minor.patch`).

## Developer Documentation

See the [detailed developer documentation](doc/devdoc.md).

## License

The [Apache 2 license](http://www.apache.org/licenses/LICENSE-2.0) (given in full in [LICENSE.txt](LICENSE.txt))
applies to all sources in this repository unless explicitly marked otherwise.
