# GeneVer Release Notes

For an explanation of the versioning scheme see [the main readme](../readme.md#versioning-scheme).

## 0.3 - User guide and improved Gradle support

Git-Tag: [v0.3](https://gitlab.com/creatorcat/genever/tags/v0.3)

### Changes

* Added detailed [user guide](userguide.md)
* Versions created via the `genever` Gradle script extension now have a default qualifier instead of `null`
* Added Gradle script support to copy versions: `copyWith { ... }`
* Added `printVersion` task to print the project version (useful to verify GeneVer config)  
* Greatly extended testing by directly verifying against Maven and Gradle version comparison classes
  and simulating Eclipse and MS-Net comparison

### Bug-fixes

* Make qualifier generation comparison stable in all cases for lexicographical qualifiers or if no metadata is defined 

### Dependency Updates

* Kotlin 1.3.20
* Kotiya H-2

### For Developers

* Apply Genever plugin to generate the version and add respective launchers to publish releases
* Add main `test` task for MPP projects that runs tests for all platforms
* Document versioning scheme and introduce latest release info in readme
* Dependencies from one MPP project to another will now lead to proper target platform specific
  artifact dependencies in generated POM files (Kotlin does not support this by itself, yet)

## 0.2 - Gradle Plugin public availability

Git-Tag: [v0.2](https://gitlab.com/creatorcat/genever/tags/v0.2)

### Changes

Genever Gradle Plugin with full functionality:
* Create versions via `genever.standardVersion { ... }`
* Generate version string via `myVersion.generate { ... }`
* Comfortable Groovy script support without the need for imports

Genever Core design improvements:
* Automatically adjust release metadata if necessary to ensure proper version comparison in all supported formats
  (final release > intermediate > snapshot)
* Added builders to easily create versions and qualifiers
* GenericVersion: `major`-, `minor`-, `patchLevel` and `levelOfBaseComponent` are now members instead of extensions

### Bug-fixes

*None*

### Dependency Updates

Main:
* Update to Kotlin 1.3.11
* Update to Gradle 5.1.1
* Update to Kotiya H-1

Test:
* Update to Kotlin-Mockito 2.0.0

### For Developers

* Removed dependency to local Kotiya artifacts in lib dir (use bintray repo instead)
* Log ci-build output into file to make it available even if gitlab truncates long outputs
* Several minor improvements of build scripts and overall documentation

## 0.1 - Core functionality

Git-Tag: [v0.1](https://gitlab.com/creatorcat/genever/tags/v0.1)

Basic version generation support:
* `GenericVersion` and `VersionQualifier` interface
* `StandardVersion` and `StandardQualifier` as default implementations supporting
  Maven, Gradle, Eclipse and MS-Net style
* Configure versions string generation via `VersionGenerationSpec`
