# GeneVer User Guide

## Apply gradle plugin 

Use the plugin in your Gradle build:

    buildscript {
        repositories {
            url = "https://dl.bintray.com/knolle/creatorcat"
        }
        dependencies {
            classpath("de.creatorcat.genever:genever-gradle:$GENEVER_VERSION")
        }
    }

    allprojects {
        apply plugin: "de.creatorcat.genever"
        
        // use genever...
    }

The plugin will add:
* The `genever` extension that provides the DSL to define and generate versions
* The `printVersion` task that simply prints the version of each project to verify the configuration

## Sample usage

Define the project version by directly generating the genever version string:

    version = genever.standardVersion {
        majorLevel = 1
        patchLevel = 3
        qualifierOf {
            phaseBeta(2)
            releaseMetadata = "vXXX"
        }
    }.generate {}

Now running `printVersion` will produce `1.0.3-b2-SNAPSHOT` because the `releaseType` is `SNAPSHOT` by default. 

The best way to build non-snapshot versions is via using the command line property `genever.releaseType`:
* `gradlew -Pgenever.releaseType=intermediate printVersion` will produce `1.0.3-b2-vXXX`
* `gradlew -Pgenever.releaseType=final printVersion` will produce `1.0.3-release-vXXX`

To publish a release of your artifact simply use the command line property when publishing:
`gradlew -Pgenever.releaseType=final publishXXX`

## DSL fully explained

### Defining versions

    // create a genever version object for further usage
    def myVersion = genever.standardVersion {
    
        // define common levels directly...
        majorLevel = 1
        minorLevel = 2
        patchLevel = 3
        // ... or set arbitrary levels via index (gaps will be filled with zeros)
        base[4] = 5
        base[7] = 8
        
        // now the base version is "1.2.3.0.5.0.0.8"
        // but it can be replaced easily... 
        base = [1, 0, 0]
        // or like this:
        base.setAll(0, 0, 1)
        
        // the base version components are empty by default:
        base = [] // default value!
         
        // the qualifier is initialized to a trivial default value, read more below...
    }

    // a version object is immutable, but it may be re-used via copy
    // note, that a version is bound to a project, which may also be replaced in the copy
    def otherVersion = myVersion.copyWith(otherProject) {
        
        // all values are initialized to those in the original source
        // now they can be redefined as necessary
        majorLevel = 5
    } 

### Defining qualifiers

    // inside the version configuration closure a qualifier can be configured
    def myVersion = genever.standardVersion {

        // A version created via the `genever`-extension functions always has a default qualifier with below values
        qualifierOf {
            phase = RELEASE_CANDIDATE // other possible values are ALPHA, BETA, MILESTONE
            phaseLevel = 1 // for defining rc1, rc2, ... 
            // metadata is appended to the generated version string for releases
            intermediateReleaseMetadata = ""
            finalReleaseMetadata = ""
        }
        
        // configuration can be simplified
        qualifierOf {
            phaseAlpha(2) // sets phase to ALPHA and phaseLevel to 2, similar to phaseBeta, phaseMilestone and phaseRC
            releaseMetadata = "vXXX" // sets both final- and intermediateReleaseMetadata
        }
        
        // qualifier may also be set directly to some object defined elsewhere
        qualifier = myOtherQualifier
    }

### Configuring generation

    // after defining some version ...
    def myVersion = genever.standardVersion { ... }
    
    // ... use the generate block to finally retrieve a string representation of the version object
    version = myVersion.generate {
        // the generation may be configured to support different version schemes
        // default values are listed below
        
        // the releaseType is best defined via command line property genever.releaseType and NOT here
        // SNAPSHOT: metadata is always SNAPSHOT
        // INTERMEDIATE: use qualifier.intermediateReleaseMetadata and development phase information
        // FINAL: use qualifier.finaleReleaseMetadata and OMIT phase information
        releaseType = SNAPSHOT
        
        preferAbbreviations = true // use abbreviations 'a' for 'alpha', 'b' for 'beta' etc.
        ensure3ComponentBase = false // extend base to have 3 components (fill with zeros) or fail if more are defined
        upCaseCharacters = false // print qualifier in uppercase
        forceNonEmptyQualifier = false // always generate a qualifier, even for final release without metadata
        
        useNumericQualifier() // generate the qualifier as number to support MS-Net style
        useLexicographicalQualifier() // ensure qualifier is lexicographically stable to support Eclipse style
        useDefaultQualifier() // the default: just use what is there
        
        useDotSeparator() // use a dot '.' to separate base and qualifier
        useHyphenSeparator() // the default 
        
        useEclipseStyle() // configure Eclipse style
        useMSNetStyle() // configure Eclipse style
        useDefaultStyle() // the default: undo above styles 
    }

## More details - the API

Module API documentation in Dokka-HTML:
* [core-module](https://creatorcat.gitlab.io/genever/docs/core/html/genever-core)
(or [javadoc](https://creatorcat.gitlab.io/genever/docs/core/javadoc))
* [gradle-plugin](https://creatorcat.gitlab.io/genever/docs/gradle/html/genever-gradle)
(or [javadoc](https://creatorcat.gitlab.io/genever/docs/gradle/javadoc))
