# Module de.creatorcat.genever.core

The core module of GeneVer defines generic platform independent types to represent software versions
supporting all common patterns. 

# Package de.creatorcat.genever.core

Contains the generic software version types and ways to easily define such objects via builders
and finally generate version strings via a generation spec.
