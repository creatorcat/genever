/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file. 
 */

package de.creatorcat.genever.core

import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import de.creatorcat.kotiya.test.core.stubbing.wasCalledWith
import de.creatorcat.kotiya.test.core.stubbing.withCallObserver
import de.creatorcat.pard.cova.annotation.VerifiesArtifact
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@ExperimentalUnsignedTypes
class GenericVersionTest {

    @Test fun `levelOfBaseComponent - usage`() {
        val version = versionWithBase(0, 1, 2)
        for (idx in 0u..2u) assertEquals(idx, version.levelOfBaseComponent(idx))
        for (idx in 3u..5u) assertEquals(0u, version.levelOfBaseComponent(idx))
    }

    @Test fun `major minor patch level - usage`() {
        val version = versionWithBase(1, 2, 3)
        assertEquals(1u, version.majorLevel)
        assertEquals(2u, version.minorLevel)
        assertEquals(3u, version.patchLevel)
    }

    @VerifiesArtifact("reqBaseVersionComparison")
    @Test fun `compareBase - with same number of components`() {
        assertTrue(versionWithBase(2).compareBase(versionWithBase(1)) > 0)
        assertTrue(versionWithBase(2).compareBase(versionWithBase(3)) < 0)
        assertEquals(0, versionWithBase(4).compareBase(versionWithBase(4)))

        assertTrue(versionWithBase(1, 2, 3).compareBase(versionWithBase(1, 2, 0)) > 0)
        assertTrue(versionWithBase(1, 2, 3).compareBase(versionWithBase(1, 2, 5)) < 0)
        assertTrue(versionWithBase(1, 2, 3).compareBase(versionWithBase(1, 3, 0)) < 0)
        assertTrue(versionWithBase(1, 2, 3).compareBase(versionWithBase(1, 1, 5)) > 0)
        assertEquals(0, versionWithBase(1, 2, 3).compareBase(versionWithBase(1, 2, 3)))
    }

    @VerifiesArtifact("reqBaseVersionComparison")
    @Test fun `compareBase - with different number of components`() {
        assertTrue(versionWithBase(2).compareBase(versionWithBase(1, 2)) > 0)
        assertTrue(versionWithBase(2).compareBase(versionWithBase(3, 0)) < 0)
        assertEquals(0, versionWithBase(4).compareBase(versionWithBase(4, 0)))

        assertTrue(versionWithBase(1, 2).compareBase(versionWithBase(1)) > 0)
        assertTrue(versionWithBase(1, 2).compareBase(versionWithBase(3)) < 0)
        assertEquals(0, versionWithBase(1, 2, 0).compareBase(versionWithBase(1, 2)))

        assertTrue(versionWithBase(1, 2, 3).compareBase(versionWithBase(1, 2)) > 0)
        assertTrue(versionWithBase(1, 2).compareBase(versionWithBase(1, 2, 3, 4)) < 0)
        assertTrue(versionWithBase(1).compareBase(versionWithBase(1, 0, 1)) < 0)
        assertTrue(versionWithBase(1, 2, 0, 0, 1).compareBase(versionWithBase(1, 2)) > 0)
        assertEquals(0, versionWithBase(1, 2, 0, 0, 0).compareBase(versionWithBase(1, 2)))
    }

    @VerifiesArtifact("reqFormatDefault", "reqFormatEclipse")
    @Test fun `generateBase - with default spec properties`() {
        val spec = VersionGenerationSpec.of {}
        assertEquals("0", versionWithBase().generateBase(spec))
        assertEquals("1", versionWithBase(1).generateBase(spec))
        assertEquals("1.2.3", versionWithBase(1, 2, 3).generateBase(spec))
        assertEquals("1.0.4.3.7", versionWithBase(1, 0, 4, 3, 7).generateBase(spec))
    }

    @VerifiesArtifact("reqFormatDefault", "reqFormatEclipse")
    @Test fun `generateBase - with 3 component property`() {
        val spec = VersionGenerationSpec.of { ensure3ComponentBase = true }
        assertEquals("0.0.0", versionWithBase().generateBase(spec))
        assertEquals("1.0.0", versionWithBase(1).generateBase(spec))
        assertEquals("1.2.3", versionWithBase(1, 2, 3).generateBase(spec))
        assertThrows<IllegalArgumentException> { versionWithBase(1, 0, 4, 3, 7).generateBase(spec) }
    }

    @VerifiesArtifact("reqVersionGeneration", "reqFormatDefault", "reqFormatEclipse")
    @Test fun `generate - with real qualifier`() {
        val version = DummyVersion(listOf(1u, 2u), DummyQualifier("Q1"))

        val spec1 = VersionGenerationSpec.of { useDotSeparator() }
        assertEquals("1.2.Q1", version.generate(spec1))
        assertThat(version.qualifier::generate, wasCalledWith(spec1))

        val spec2 = VersionGenerationSpec.of { ensure3ComponentBase = true }
        assertEquals("1.2.0-Q1", version.generate(spec2))
        assertThat(version.qualifier::generate, wasCalledWith(spec2))
    }

    @VerifiesArtifact("reqVersionGeneration")
    @Test fun `generate - with blank qualifier`() {
        val version = DummyVersion(listOf(1u, 2u), DummyQualifier(""))

        assertEquals("1.2", version.generate {})
    }

    @VerifiesArtifact("reqVersionComparison")
    @Test fun `compareTo - depends on base version first`() {
        val qualifier = DummyQualifier(compareResult = 1)
        val v1 = DummyVersion(listOf(1u, 1u), qualifier)
        val v2 = DummyVersion(listOf(1u, 2u), qualifier)
        val v3 = DummyVersion(listOf(1u, 3u), qualifier)
        assertTrue(v1 < v2)
        assertTrue(v2 > v1)
        assertTrue(v1 < v3)
        assertTrue(v3 > v1)
        assertTrue(v2 < v3)
        assertTrue(v3 > v2)
    }

    @VerifiesArtifact("reqVersionComparison")
    @Test fun `compareTo - depends on qualifier if base equal`() {
        val v1 = DummyVersion(listOf(1u), DummyQualifier(compareResult = 0))
        val v2 = DummyVersion(listOf(1u), DummyQualifier(compareResult = 1))
        val v3 = DummyVersion(listOf(1u), DummyQualifier(compareResult = -1))
        assertEquals(0, v1.compareTo(v2))
        assertEquals(0, v1.compareTo(v3))
        assertTrue(v2 > v1)
        assertTrue(v2 > v3)
        assertTrue(v3 < v1)
        assertTrue(v3 < v2)
    }

    private fun versionWithBase(vararg components: Int): DummyVersion =
        DummyVersion(components.map(Int::toUInt), DummyQualifier())

    private class DummyQualifier(
        val generateResult: String = "dummy",
        val compareResult: Int = 0
    ) : VersionQualifier<DummyQualifier> {
        override fun equals(other: Any?) = mustNotBeCalled()
        override fun hashCode() = mustNotBeCalled()

        override fun generate(spec: VersionGenerationSpec): String =
            withCallObserver(::generate, spec) { generateResult }

        override fun compareTo(other: DummyQualifier): Int =
            withCallObserver(::compareTo, other) { compareResult }
    }

    private class DummyVersion(
        override val base: List<UInt>,
        override val qualifier: DummyQualifier
    ) : GenericVersion<DummyQualifier> {
        override fun equals(other: Any?) = mustNotBeCalled()
        override fun hashCode() = mustNotBeCalled()
    }
}
