/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.core.reqtest

import de.creatorcat.genever.core.StandardQualifier
import de.creatorcat.genever.core.StandardVersionBuilder
import de.creatorcat.genever.core.VersionGenerationSpecBuilder
import de.creatorcat.genever.core.copyWith
import de.creatorcat.kotiya.matchers.common.isEqual
import de.creatorcat.kotiya.test.core.assertThat

/**
 * Version comparison test simulating the Eclipse versioning scheme.
 */
@ExperimentalUnsignedTypes
class MSBuildStdVersionComparisonTest : StandardVersionComparisonTestBase<MSBuildEquivalentVersion>() {
    override fun adjustBuilderIfNecessary(builder: StandardVersionBuilder<StandardQualifier>) {
        builder.qualifier = builder.qualifier?.copyWith {
            finalReleaseMetadata = ensureNumericMetadata(finalReleaseMetadata)
            intermediateReleaseMetadata = ensureNumericMetadata(intermediateReleaseMetadata)
            phaseLevel = minOf(9u, phaseLevel)
        }
    }

    private fun ensureNumericMetadata(metadata: String): String {
        if (metadata.matches(Regex("\\d*")))
            return metadata
        return (metadata.hashCode() % 1000).toString()
    }

    override fun provideBasicGenerationSpec(): VersionGenerationSpecBuilder {
        return VersionGenerationSpecBuilder().apply {
            useMSNetStyle()
        }
    }

    override fun generateComparableVersion(versionString: String): MSBuildEquivalentVersion {
        val parts = versionString.split('.')
        assertThat("version parts separated by '.'", parts.size, isEqual(4))
        return MSBuildEquivalentVersion(
            parts[0].toInt(), parts[1].toInt(), parts[2].toInt(), parts[3].toInt()
        )
    }
}

@ExperimentalUnsignedTypes
class MSBuildEquivalentVersion(
    private val major: Int, private val minor: Int, private val patch: Int, private val qualifier: Int
) : Comparable<MSBuildEquivalentVersion> {

    override fun compareTo(other: MSBuildEquivalentVersion): Int {
        var result = this.major - other.major
        if (result == 0) result = this.minor - other.minor
        if (result == 0) result = this.patch - other.patch
        if (result == 0) result = this.qualifier - other.qualifier
        return result
    }

} 
