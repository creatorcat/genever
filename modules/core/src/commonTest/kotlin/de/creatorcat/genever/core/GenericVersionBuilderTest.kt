/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.core

import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.matchers.common.isNull
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import de.creatorcat.pard.cova.annotation.VerifiesArtifact
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame

@ExperimentalUnsignedTypes
class GenericVersionBuilderTest {

    private val testee = DummyBuilder()
    private val base = MutableBaseComponents()

    @VerifiesArtifact("reqBaseVersionDef")
    @Test fun `BaseComponents - setAll`() {
        base.setAll(1u, 2u, 3u)
        assertThat(base.components, hasElementsEqual(1u, 2u, 3u))
        base.setAll(17u)
        assertThat(base.components, hasElementsEqual(17u))
    }

    @VerifiesArtifact("reqBaseVersionDef")
    @Test fun `BaseComponents - get`() {
        base.setAll(1u, 2u, 3u)
        assertEquals(1u, base[0u])
        assertEquals(2u, base[1u])
        assertEquals(3u, base[2u])
        assertThrows<IndexOutOfBoundsException> { base[3u] }
    }

    @VerifiesArtifact("reqBaseVersionDef")
    @Test fun `BaseComponents - set`() {
        base[2u] = 3u
        assertThat(base.components, hasElementsEqual(0u, 0u, 3u))
        base[0u] = 1u
        assertThat(base.components, hasElementsEqual(1u, 0u, 3u))
        base[4u] = 5u
        assertThat(base.components, hasElementsEqual(1u, 0u, 3u, 0u, 5u))
    }

    @Test fun `base - default`() {
        with(testee) {
            assertThat(base.components, isEmpty)
        }
    }

    @Test fun `majorLevel - default, get and set`() {
        with(testee) {
            assertThrows<IndexOutOfBoundsException> { majorLevel }
            majorLevel = 1u
            assertEquals(1u, majorLevel)
            assertThat(base.components, hasElementsEqual(1u))
            base.setAll(3u, 2u, 1u)
            assertEquals(3u, majorLevel)
        }
    }

    @Test fun `minorLevel - default, get and set`() {
        with(testee) {
            assertThrows<IndexOutOfBoundsException> { minorLevel }
            minorLevel = 2u
            assertEquals(2u, minorLevel)
            assertThat(base.components, hasElementsEqual(0u, 2u))
            base.setAll(3u, 3u, 3u)
            assertEquals(3u, minorLevel)
        }
    }

    @Test fun `patchLevel - default, get and set`() {
        with(testee) {
            assertThrows<IndexOutOfBoundsException> { patchLevel }
            patchLevel = 1u
            assertEquals(1u, patchLevel)
            assertThat(base.components, hasElementsEqual(0u, 0u, 1u))
            base.setAll(1u, 2u, 3u)
            assertEquals(3u, patchLevel)
        }
    }

    @VerifiesArtifact("reqVersionDef")
    @Test fun `qualifier - default, get and set`() {
        with(testee) {
            assertThat(qualifier, isNull)
            val q = DummyQualifier()
            qualifier = q
            assertSame(q, qualifier)
        }
    }

    private class DummyBuilder : GenericVersionBuilder<DummyQualifier, GenericVersion<DummyQualifier>>() {
        override fun build() = mustNotBeCalled()
    }

    private class DummyQualifier : VersionQualifier<DummyQualifier> {
        override fun generate(spec: VersionGenerationSpec) = mustNotBeCalled()
        override fun compareTo(other: DummyQualifier) = mustNotBeCalled()
        override fun equals(other: Any?) = mustNotBeCalled()
        override fun hashCode() = mustNotBeCalled()
    }
}
