/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.core

import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import de.creatorcat.pard.cova.annotation.VerifiesArtifact
import kotlin.test.Test
import kotlin.test.assertNotSame
import kotlin.test.assertSame

@ExperimentalUnsignedTypes
class StandardVersionBuilderTest {

    @VerifiesArtifact("reqVersionDef")
    @Test fun `build - standard usage`() {
        val q = DummyQualifier()
        val version = with(StandardVersionBuilder<DummyQualifier>()) {
            qualifier = q
            base.setAll(1u, 2u, 3u)
            build()
        }
        assertThat(version.base, hasElementsEqual(1u, 2u, 3u))
        assertSame(q, version.qualifier)
    }

    @VerifiesArtifact("reqVersionDef")
    @Test fun `build - missing qualifier`() {
        assertThrows<IllegalStateException> {
            StandardVersionBuilder<DummyQualifier>().build()
        }
    }

    @Test fun `build - result is unique and independent`() {
        val q1 = DummyQualifier()
        val q2 = DummyQualifier()
        val builder = StandardVersionBuilder<DummyQualifier>()
        val version1 = with(builder) {
            qualifier = q1
            base.setAll(1u, 2u, 3u)
            build()
        }
        val version2 = with(builder) {
            majorLevel = 2u
            qualifier = q2
            build()
        }
        assertThat(version1.base, hasElementsEqual(1u, 2u, 3u))
        assertThat(version2.base, hasElementsEqual(2u, 2u, 3u))
        assertSame(q1, version1.qualifier)
        assertSame(q2, version2.qualifier)
        assertNotSame(version1, version2)
    }

    @Test fun `of - standard usage`() {
        val q = DummyQualifier()
        val version = StandardVersion.of<DummyQualifier> {
            qualifier = q
            base.setAll(1u, 2u, 3u)
        }
        assertThat(version.base, hasElementsEqual(1u, 2u, 3u))
        assertSame(q, version.qualifier)
    }

    @Test fun `copyWith - standard usage`() {
        val q = DummyQualifier()
        val original = StandardVersion(q, listOf(1u, 2u, 3u))
        val copy = original.copyWith {
            majorLevel = 2u
            patchLevel = 0u
        }
        assertThat(original.base, hasElementsEqual(1u, 2u, 3u))
        assertSame(q, original.qualifier)
        assertThat(copy.base, hasElementsEqual(2u, 2u, 0u))
        assertSame(q, copy.qualifier)
    }

    private class DummyQualifier : VersionQualifier<DummyQualifier> {
        override fun generate(spec: VersionGenerationSpec) = mustNotBeCalled()
        override fun compareTo(other: DummyQualifier) = mustNotBeCalled()
        override fun equals(other: Any?) = mustNotBeCalled()
        override fun hashCode() = mustNotBeCalled()
    }
}
