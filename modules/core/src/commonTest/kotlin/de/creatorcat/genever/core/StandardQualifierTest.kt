/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.core

import de.creatorcat.genever.core.ReleaseType.FINAL
import de.creatorcat.genever.core.ReleaseType.INTERMEDIATE
import de.creatorcat.genever.core.ReleaseType.SNAPSHOT
import de.creatorcat.genever.core.StandardQualifier.Phase
import de.creatorcat.genever.core.StandardQualifier.Phase.ALPHA
import de.creatorcat.genever.core.StandardQualifier.Phase.BETA
import de.creatorcat.genever.core.StandardQualifier.Phase.MILESTONE
import de.creatorcat.genever.core.StandardQualifier.Phase.RELEASE_CANDIDATE
import de.creatorcat.kotiya.core.structures.uSize
import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import de.creatorcat.kotiya.test.core.reflect.assertCompareToContractFor
import de.creatorcat.kotiya.test.core.reflect.assertCompareToEquivalenceProperty
import de.creatorcat.pard.cova.annotation.VerifiesArtifact
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@ExperimentalUnsignedTypes
class StandardQualifierTest {

    @VerifiesArtifact("reqQualifierMetaData", comment = "metadata is empty by default")
    @Test fun `creation - default values`() {
        with(StandardQualifier()) {
            assertEquals(RELEASE_CANDIDATE, phase)
            assertEquals(1u, phaseLevel)
            assertThat(finalReleaseMetadata, isEmpty)
            assertThat(intermediateReleaseMetadata, isEmpty)
        }
    }

    @Test fun `phase - max number not exceeded`() {
        assertTrue(StandardQualifier.maxPhaseNumber >= Phase.values().uSize)
    }

    @VerifiesArtifact("reqQualifierComparison")
    @Test fun `compareTo - functionality`() {
        assertEquals(0, StandardQualifier(ALPHA, 1u, "final").compareTo(StandardQualifier(ALPHA, 1u)))
        assertTrue(StandardQualifier(ALPHA, 2u) > StandardQualifier(ALPHA, 1u))
        assertTrue(StandardQualifier(BETA, 1u) > StandardQualifier(ALPHA, 1u))
        assertTrue(StandardQualifier(BETA, 5u) < StandardQualifier(MILESTONE, 1u))
        assertTrue(StandardQualifier(MILESTONE, 5u) < StandardQualifier(RELEASE_CANDIDATE, 1u))
    }

    @VerifiesArtifact("reqQualifierComparison")
    @Test fun `compareTo - general contract`() {
        val testObjects = listOf(
            // some comparison-equals
            StandardQualifier(ALPHA, 1u, "final"), StandardQualifier(ALPHA, 1u, "v1", "vI"),
            StandardQualifier(ALPHA, 1u, intermediateReleaseMetadata = "r123"),
            // some non-equals
            StandardQualifier(BETA, 2u), StandardQualifier(BETA, 3u),
            StandardQualifier(MILESTONE)
        )
        StandardQualifier::compareTo.assertCompareToContractFor(testObjects)
        StandardQualifier::compareTo.assertCompareToEquivalenceProperty(testObjects)
    }

    @VerifiesArtifact("reqFormatDefault", "reqFormatEclipse")
    @Test fun `generate - intermediate release - phase part`() {
        assertEquals("a1", StandardQualifier(ALPHA).generate { releaseType = INTERMEDIATE })
        assertEquals("b4", StandardQualifier(BETA, 4u).generate { releaseType = INTERMEDIATE })

        assertEquals("alpha1", StandardQualifier(ALPHA).generate {
            releaseType = INTERMEDIATE
            preferAbbreviations = false
        })
        assertEquals("milestone244", StandardQualifier(MILESTONE, 244u).generate {
            releaseType = INTERMEDIATE
            preferAbbreviations = false
        })
    }

    @VerifiesArtifact("reqFormatDefault")
    @Test fun `generate - intermediate release - meta-data part - non-lexicographical`() {
        val spec = VersionGenerationSpec.of { releaseType = INTERMEDIATE }

        assertEquals("a1-xyz", StandardQualifier(ALPHA, intermediateReleaseMetadata = "xyz").generate(spec))
        assertEquals("a1-123", StandardQualifier(ALPHA, intermediateReleaseMetadata = "123").generate(spec))
        assertEquals("a1-vMD", StandardQualifier(ALPHA, intermediateReleaseMetadata = "MD").generate(spec))
        assertEquals("a1", StandardQualifier(ALPHA).generate(spec))
    }

    @VerifiesArtifact("reqFormatEclipse")
    @Test fun `generate - intermediate release - meta-data part - lexicographical`() {
        val spec = VersionGenerationSpec.of {
            releaseType = INTERMEDIATE
            useLexicographicalQualifier()
        }

        assertEquals("a001-xyz", StandardQualifier(ALPHA, intermediateReleaseMetadata = "xyz").generate(spec))
        assertEquals("a001-v123", StandardQualifier(ALPHA, intermediateReleaseMetadata = "123").generate(spec))
        assertEquals("a001-vMD", StandardQualifier(ALPHA, intermediateReleaseMetadata = "MD").generate(spec))
        assertEquals("a001-vFinal", StandardQualifier(ALPHA).generate(spec))
    }

    @Test fun `generate - snapshot release`() {
        assertEquals("a1-SNAPSHOT", StandardQualifier(ALPHA).generate { })
    }

    @Test fun `generate - final release`() {
        assertEquals("", StandardQualifier().generate { releaseType = FINAL })

        assertEquals(
            "release",
            StandardQualifier().generate {
                releaseType = FINAL
                forceNonEmptyQualifier = true
            }
        )

        assertEquals(
            "release-v123",
            StandardQualifier(finalReleaseMetadata = "v123").generate { releaseType = FINAL }
        )

        assertEquals(
            "123",
            StandardQualifier(finalReleaseMetadata = "123").generate { releaseType = FINAL }
        )

        assertEquals(
            "release-123",
            StandardQualifier(finalReleaseMetadata = "123").generate {
                releaseType = FINAL
                useLexicographicalQualifier()
            }
        )
    }

    @VerifiesArtifact("reqFormatEclipse")
    @Test fun `generate - lexicographical phase level`() {
        val spec = VersionGenerationSpec.of {
            releaseType = INTERMEDIATE
            useLexicographicalQualifier()
        }
        assertEquals("a003-X", StandardQualifier(ALPHA, 3u, intermediateReleaseMetadata = "X").generate(spec))
        assertEquals("a123-X", StandardQualifier(ALPHA, 123u, intermediateReleaseMetadata = "X").generate(spec))

        assertThrows<IllegalArgumentException> {
            StandardQualifier(ALPHA, 1234u).generate(spec)
        }
    }

    @Test fun `generate as number - final release`() {
        val spec = VersionGenerationSpec.of {
            releaseType = FINAL
            useNumericQualifier()
        }
        assertEquals("61000", StandardQualifier().generate(spec))
        assertEquals("67000", StandardQualifier(phaseLevel = 7u).generate(spec))
        assertEquals("61321", StandardQualifier(finalReleaseMetadata = "321").generate(spec))

        assertThrows<IllegalArgumentException> {
            StandardQualifier(phaseLevel = 11u).generate(spec)
        }
        assertThrows<IllegalArgumentException> {
            StandardQualifier(finalReleaseMetadata = "a").generate(spec)
        }
        assertThrows<IllegalArgumentException> {
            StandardQualifier(finalReleaseMetadata = "1234").generate(spec)
        }
    }

    @Test fun `generate as number - intermediate release`() {
        val spec = VersionGenerationSpec.of {
            releaseType = INTERMEDIATE
            useNumericQualifier()
        }
        assertEquals("01100", StandardQualifier(ALPHA).generate(spec))
        assertEquals("17100", StandardQualifier(BETA, 7u).generate(spec))
        assertEquals("33100", StandardQualifier(RELEASE_CANDIDATE, 3u).generate(spec))
        assertEquals("31021", StandardQualifier(intermediateReleaseMetadata = "21").generate(spec))
        assertEquals("31100", StandardQualifier(intermediateReleaseMetadata = "0").generate(spec))

        assertThrows<IllegalArgumentException> {
            StandardQualifier(phaseLevel = 11u).generate(spec)
        }
        assertThrows<IllegalArgumentException> {
            StandardQualifier(intermediateReleaseMetadata = "a").generate(spec)
        }
        assertThrows<IllegalArgumentException> {
            StandardQualifier(intermediateReleaseMetadata = "1234").generate(spec)
        }
    }

    @Test fun `generate as number - snapshot release`() {
        val spec = VersionGenerationSpec.of {
            releaseType = SNAPSHOT
            useNumericQualifier()
        }
        assertEquals("01000", StandardQualifier(ALPHA).generate(spec))
        assertEquals("17000", StandardQualifier(BETA, 7u).generate(spec))
        assertEquals("33000", StandardQualifier(RELEASE_CANDIDATE, 3u).generate(spec))

        assertThrows<IllegalArgumentException> {
            StandardQualifier(phaseLevel = 11u).generate(spec)
        }
    }

    @Test fun `generate - with up casing`() {
        assertEquals("ALPHA1-V123", StandardQualifier(ALPHA, intermediateReleaseMetadata = "v123").generate {
            releaseType = INTERMEDIATE
            preferAbbreviations = false
            upCaseCharacters = true
        })
        assertEquals("RELEASE-V123", StandardQualifier(finalReleaseMetadata = "v123").generate {
            releaseType = FINAL
            upCaseCharacters = true
        })
    }
}
