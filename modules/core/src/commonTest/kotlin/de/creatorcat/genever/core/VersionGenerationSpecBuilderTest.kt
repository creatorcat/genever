/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file. 
 */

package de.creatorcat.genever.core

import de.creatorcat.genever.core.ReleaseType.FINAL
import de.creatorcat.genever.core.ReleaseType.INTERMEDIATE
import de.creatorcat.genever.core.ReleaseType.SNAPSHOT
import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import de.creatorcat.kotiya.test.core.stubbing.observedArguments
import de.creatorcat.kotiya.test.core.stubbing.withCallObserver
import de.creatorcat.pard.cova.annotation.VerifiesArtifact
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotSame
import kotlin.test.assertTrue

@ExperimentalUnsignedTypes
@VerifiesArtifact("reqVersionGeneration")
class VersionGenerationSpecBuilderTest {

    private class DummyQualifier : VersionQualifier<DummyQualifier> {
        override fun compareTo(other: DummyQualifier) = mustNotBeCalled()
        override fun equals(other: Any?) = mustNotBeCalled()
        override fun hashCode() = mustNotBeCalled()

        override fun generate(spec: VersionGenerationSpec): String = withCallObserver(::generate, spec) {
            "DUMMY"
        }
    }

    private class DummyVersion : GenericVersion<DummyQualifier> {
        override val base get() = mustNotBeCalled()
        override val qualifier get() = mustNotBeCalled()
        override fun equals(other: Any?) = mustNotBeCalled()
        override fun hashCode() = mustNotBeCalled()

        override fun generate(spec: VersionGenerationSpec): String = withCallObserver(::generate, spec) {
            "DUMMY"
        }
    }

    private val testee = VersionGenerationSpecBuilder()

    @Test fun `convenience version generate`() {
        val version = DummyVersion()
        version.generate {
            preferAbbreviations = false
            releaseType = FINAL
        }

        val argList = version::generate.observedArguments
        assertEquals(1, argList.size)
        val spec = argList[0][0] as VersionGenerationSpec
        assertFalse(spec.preferAbbreviations)
        assertEquals(FINAL, spec.releaseType)
    }

    @Test fun `convenience qualifier generate`() {
        val qualifier = DummyQualifier()
        qualifier.generate {
            preferAbbreviations = false
            releaseType = INTERMEDIATE
        }

        val argList = qualifier::generate.observedArguments
        assertEquals(1, argList.size)
        val spec = argList[0][0] as VersionGenerationSpec
        assertFalse(spec.preferAbbreviations)
        assertEquals(INTERMEDIATE, spec.releaseType)
    }

    @Test fun `convenience creation of`() {
        val spec = VersionGenerationSpec.of {
            preferAbbreviations = false
            releaseType = FINAL
        }

        assertFalse(spec.preferAbbreviations)
        assertEquals(FINAL, spec.releaseType)
    }

    @Test fun `releaseType - usage and defaults`() {
        assertEquals(SNAPSHOT, testee.releaseType)
        testee.releaseType = FINAL
        assertEquals(FINAL, testee.build().releaseType)
    }

    @VerifiesArtifact("reqFormatDefault", "reqFormatEclipse")
    @Test fun `preferAbbreviations - usage and defaults`() {
        assertTrue(testee.preferAbbreviations)
        testee.preferAbbreviations = false
        assertFalse(testee.build().preferAbbreviations)
    }

    @VerifiesArtifact("reqFormatEclipse")
    @Test fun `ensure3ComponentBase - usage and defaults`() {
        assertFalse(testee.ensure3ComponentBase)
        testee.ensure3ComponentBase = true
        assertTrue(testee.build().ensure3ComponentBase)
    }

    @Test fun `upCaseCharacters - usage and defaults`() {
        assertFalse(testee.upCaseCharacters)
        testee.upCaseCharacters = true
        assertTrue(testee.build().upCaseCharacters)
    }

    @VerifiesArtifact("reqFormatEclipse")
    @Test fun `forceNonEmptyQualifier - usage and defaults`() {
        assertFalse(testee.forceNonEmptyQualifier)
        testee.forceNonEmptyQualifier = true
        assertTrue(testee.build().forceNonEmptyQualifier)
    }

    @Test fun `use numeric qualifier`() {
        testee.useNumericQualifier()
        assertNumericQualifier()
    }

    @VerifiesArtifact("reqFormatEclipse")
    @Test fun `use lexicographical qualifier`() {
        testee.useLexicographicalQualifier()
        assertLexicographicalQualifier()
    }

    @VerifiesArtifact("reqFormatDefault")
    @Test fun `use default qualifier`() {
        testee.useDefaultQualifier()
        assertDefaultQualifier()
    }

    @VerifiesArtifact("reqFormatDefault")
    @Test fun `use hyphen separator`() {
        testee.useHyphenSeparator()
        assertEquals("-", testee.build().qualifierSeparator)
    }

    @VerifiesArtifact("reqFormatEclipse")
    @Test fun `use dot separator`() {
        testee.useDotSeparator()
        assertEquals(".", testee.build().qualifierSeparator)
    }

    @VerifiesArtifact("reqFormatEclipse")
    @Test fun `use eclipse style`() {
        testee.useEclipseStyle()
        assertLexicographicalQualifier()
        with(testee.build()) {
            assertEquals(".", qualifierSeparator)
            assertTrue(ensure3ComponentBase)
            assertTrue(forceNonEmptyQualifier)
        }
    }

    @Test fun `use ms-net style`() {
        testee.useMSNetStyle()
        assertNumericQualifier()
        with(testee.build()) {
            assertEquals(".", qualifierSeparator)
            assertTrue(ensure3ComponentBase)
        }
    }

    @VerifiesArtifact("reqFormatDefault")
    @Test fun `use default style`() {
        testee.useEclipseStyle() // overwrite defaults first
        testee.useDefaultStyle()
        assertDefaultStyle()
    }

    @Test fun `default style is default`() {
        assertDefaultStyle()
    }

    private fun assertDefaultStyle() {
        assertDefaultQualifier()
        with(testee.build()) {
            assertEquals("-", qualifierSeparator)
            assertFalse(ensure3ComponentBase)
            assertFalse(forceNonEmptyQualifier)
        }
    }

    private fun assertNumericQualifier() {
        with(testee.build()) {
            assertTrue(qualifierAsNumber)
            assertFalse(lexicographicalQualifier)
        }
    }

    private fun assertLexicographicalQualifier() {
        with(testee.build()) {
            assertFalse(qualifierAsNumber)
            assertTrue(lexicographicalQualifier)
        }
    }

    private fun assertDefaultQualifier() {
        with(testee.build()) {
            assertFalse(qualifierAsNumber)
            assertFalse(lexicographicalQualifier)
        }
    }

    @Test fun `build produces unique objects`() {
        assertNotSame(testee.build(), testee.build())
    }

    @Test fun `build produces immutable objects`() {
        val spec = testee.build()
        testee.releaseType = FINAL
        testee.useMSNetStyle()
        testee.preferAbbreviations = false
        with(spec) {
            assertEquals(SNAPSHOT, releaseType)
            assertEquals("-", qualifierSeparator)
            assertTrue(preferAbbreviations)
            assertFalse(ensure3ComponentBase)
            assertFalse(lexicographicalQualifier)
            assertFalse(qualifierAsNumber)
        }
    }

    @Test fun `build spec has data class props`() {
        val spec1 = testee.build()
        val spec2 = testee.build()
        assertThat(spec1.toString(), contains("releaseType=SNAPSHOT"))
        assertEquals(spec1, spec2)
        assertEquals(spec1.hashCode(), spec2.hashCode())
    }
}
