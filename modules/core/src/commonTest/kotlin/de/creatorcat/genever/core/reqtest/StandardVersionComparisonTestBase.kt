/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.core.reqtest

import de.creatorcat.genever.core.ReleaseType
import de.creatorcat.genever.core.ReleaseType.FINAL
import de.creatorcat.genever.core.ReleaseType.INTERMEDIATE
import de.creatorcat.genever.core.ReleaseType.SNAPSHOT
import de.creatorcat.genever.core.StandardQualifier
import de.creatorcat.genever.core.StandardVersionBuilder
import de.creatorcat.genever.core.VersionGenerationSpecBuilder
import kotlin.test.Test
import kotlin.test.assertTrue

/**
 * Base class for comparison tests performed on a set of generated test versions
 * based on [getOrderedStandardVersionBuilders].
 * The versions will be compared in a transitive row i.e.
 * it is verified that `a < b`, `b < c`, `c < d` etc.
 *
 * The actual tests only need to implement the abstract methods
 * and may override [adjustBuilderIfNecessary].
 *
 * @param V class of comparable version objects suitable for the actual kind of test
 *   e.g. Eclipse style versions or Gradle version objects.
 */
@ExperimentalUnsignedTypes
abstract class StandardVersionComparisonTestBase<V : Comparable<V>> {

    private val specBuilder by lazy(::provideBasicGenerationSpec)

    @Test fun `verify version order for whole test data list`() {
        val smallestVersionBuilder = getSmallestVersionBuilder()
        adjustBuilderIfNecessary(smallestVersionBuilder)
        var lastGreatest: Pair<String, V> = smallestVersionBuilder.build(FINAL)

        for (versionBuilderList in getOrderedStandardVersionBuilders()) {
            for (versionBuilder in versionBuilderList) {
                adjustBuilderIfNecessary(versionBuilder)

                val snapshot = versionBuilder.build(SNAPSHOT)
                val intermediate = versionBuilder.build(INTERMEDIATE)

                lastGreatest.assertLessThan(snapshot)
                snapshot.assertLessThan(intermediate)

                lastGreatest = intermediate
            }
            val final = versionBuilderList[0].build(FINAL)
            lastGreatest.assertLessThan(final)

            lastGreatest = final
        }
    }

    private fun Pair<String, V>.assertLessThan(other: Pair<String, V>) {
        logVersionComparison("Comparing: ${this.first} < ${other.first}")
        assertTrue(
            this.second < other.second,
            "Comparison condition not satisfied: ${this.first} < ${other.first}"
        )
    }

    private fun StandardVersionBuilder<StandardQualifier>.build(releaseType: ReleaseType): Pair<String, V> {
        specBuilder.releaseType = releaseType
        val versionString = build().generate(specBuilder.build())
        return Pair(versionString, generateComparableVersion(versionString))
    }

    /**
     * Perform adjustment on the version [builder] to make it produce proper versions for this kind of test.
     *
     * *Caution*: The builders are pre-configured to produce proper versions in ascending order,
     * so re-configuration should only be done if absolutely necessary and keeping in mind not
     * to alter the order.
     */
    protected open fun adjustBuilderIfNecessary(builder: StandardVersionBuilder<StandardQualifier>) {}

    /**
     * Create a generation spec that takes the specific type of test into account
     * e.g. Eclipse style or MS-Build style.
     * Note, that the release type will be adjusted during test run to generate different release versions.
     */
    protected abstract fun provideBasicGenerationSpec(): VersionGenerationSpecBuilder

    /**
     * Generate the test kind specific comparable version object from a string
     * as it will be done by the actual framework under test e.g. Gradle, Maven, Eclipse.
     */
    protected abstract fun generateComparableVersion(versionString: String): V
}

/**
 * Log the given text line to a special platform specific version comparison logger (e.g. a file).
 */
internal expect fun logVersionComparison(line: String)
