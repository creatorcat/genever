/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.core.usecases

import de.creatorcat.genever.core.ReleaseType.FINAL
import de.creatorcat.genever.core.ReleaseType.INTERMEDIATE
import de.creatorcat.genever.core.ReleaseType.SNAPSHOT
import de.creatorcat.genever.core.StandardQualifier
import de.creatorcat.genever.core.StandardQualifier.Phase.BETA
import de.creatorcat.genever.core.StandardVersion
import de.creatorcat.genever.core.generate
import de.creatorcat.genever.core.of
import de.creatorcat.genever.core.qualifierOf
import de.creatorcat.pard.cova.annotation.VerifiesArtifact
import kotlin.test.Test
import kotlin.test.assertEquals

/**
 * Use case tests concerning [StandardVersion] and [StandardQualifier].
 */
@ExperimentalUnsignedTypes
class StandardVersionUseCaseTest {

    @VerifiesArtifact(
        "reqVersionDef", "reqBaseVersionDef", "reqQualifierPhase", "reqQualifierMetaData",
        "reqVersionGeneration"
    )
    @Test fun `simple maven style project`() {
        val qualifier = StandardQualifier(BETA, 3u, intermediateReleaseMetadata = revNum.toString())
        val version = StandardVersion(qualifier, listOf(1u, 1u))

        // generate for beta release
        val betaVersion = version.generate {
            releaseType = INTERMEDIATE
            preferAbbreviations = false
            ensure3ComponentBase = true
        }
        assertEquals("1.1.0-beta3-321", betaVersion)

        // generate for final release
        val finalVersion = version.generate {
            releaseType = FINAL
            ensure3ComponentBase = true
        }
        assertEquals("1.1.0", finalVersion)
    }

    @VerifiesArtifact(
        "reqVersionDef", "reqBaseVersionDef", "reqQualifierPhase", "reqQualifierMetaData",
        "reqVersionGeneration", "reqFormatEclipse"
    )
    @Test fun `complex multi-style project`() {
        val qualifier = StandardQualifier(BETA, 3u, intermediateReleaseMetadata = revNum.toString())
        val version = StandardVersion(qualifier, listOf(1u, 2u))

        // for maven jars
        val mavenVersion = version.generate {
            releaseType = INTERMEDIATE
        }
        assertEquals("1.2-b3-321", mavenVersion)

        // for eclipse plugins
        val eclipseVersion = version.generate {
            releaseType = FINAL
            useEclipseStyle()
        }
        assertEquals("1.2.0.release", eclipseVersion)

        // for ms-build assemblies
        val msBuildVersion = version.generate {
            releaseType = SNAPSHOT
            useMSNetStyle()
        }
        assertEquals("1.2.0.13000", msBuildVersion)
    }

    @VerifiesArtifact(
        "reqVersionDef", "reqBaseVersionDef", "reqQualifierPhase", "reqQualifierMetaData",
        "reqVersionGeneration"
    )
    @Test fun `simple maven style with builders`() {
        val version = StandardVersion.of<StandardQualifier> {
            majorLevel = 1u
            patchLevel = 2u
            qualifierOf {
                phaseBeta(3u)
                intermediateReleaseMetadata = revNum.toString()
            }
        }

        // generate for beta release
        val betaVersion = version.generate {
            releaseType = INTERMEDIATE
            preferAbbreviations = false
            ensure3ComponentBase = true
        }
        assertEquals("1.0.2-beta3-321", betaVersion)

        // generate for final release
        val finalVersion = version.generate {
            releaseType = FINAL
            ensure3ComponentBase = true
        }
        assertEquals("1.0.2", finalVersion)
    }

    private val revNum = 321
}
