/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.core

import de.creatorcat.genever.core.StandardQualifier.Phase
import de.creatorcat.genever.core.StandardQualifier.Phase.ALPHA
import de.creatorcat.genever.core.StandardQualifier.Phase.BETA
import de.creatorcat.genever.core.StandardQualifier.Phase.MILESTONE
import de.creatorcat.genever.core.StandardQualifier.Phase.RELEASE_CANDIDATE
import de.creatorcat.pard.cova.annotation.VerifiesArtifact
import kotlin.test.Test
import kotlin.test.assertEquals

@ExperimentalUnsignedTypes
class StandardQualifierBuilderTest {

    private val testee = StandardQualifierBuilder()

    @VerifiesArtifact("reqQualifierPhase")
    @Test fun `phase - default, get and set`() {
        testee.assert(phase = RELEASE_CANDIDATE)
        testee.phase = ALPHA
        testee.assert(phase = ALPHA)
    }

    @VerifiesArtifact("reqQualifierPhase")
    @Test fun `phaseLevel - default, get and set`() {
        testee.assert(phaseLevel = 1u)
        testee.phaseLevel = 3u
        testee.assert(phaseLevel = 3u)
    }

    @VerifiesArtifact("reqQualifierMetaData")
    @Test fun `finalReleaseMetadata - default, get and set`() {
        testee.assert(finalReleaseMetadata = "")
        testee.finalReleaseMetadata = "MD"
        testee.assert(finalReleaseMetadata = "MD")
    }

    @VerifiesArtifact("reqQualifierMetaData")
    @Test fun `intermediateReleaseMetadata - default, get and set`() {
        testee.assert(intermediateReleaseMetadata = "")
        testee.intermediateReleaseMetadata = "MD"
        testee.assert(intermediateReleaseMetadata = "MD")
    }

    @Test fun `releaseMetadata - default, get and set`() {
        testee.assert(releaseMetadata = "")
        testee.releaseMetadata = "MD"
        testee.assert(releaseMetadata = "MD", finalReleaseMetadata = "MD", intermediateReleaseMetadata = "MD")
    }

    @Test fun `phase shortcuts - alpha`() {
        testee.phaseAlpha()
        testee.assert(phase = ALPHA)
        testee.phaseAlpha(3u)
        testee.assert(phase = ALPHA, phaseLevel = 3u)
    }

    @Test fun `phase shortcuts - beta`() {
        testee.phaseBeta()
        testee.assert(phase = BETA)
        testee.phaseBeta(3u)
        testee.assert(phase = BETA, phaseLevel = 3u)
    }

    @Test fun `phase shortcuts - milestone`() {
        testee.phaseMilestone()
        testee.assert(phase = MILESTONE)
        testee.phaseMilestone(3u)
        testee.assert(phase = MILESTONE, phaseLevel = 3u)
    }

    @Test fun `phase shortcuts - release candidate`() {
        testee.phaseRC()
        testee.assert(phase = RELEASE_CANDIDATE)
        testee.phaseRC(3u)
        testee.assert(phase = RELEASE_CANDIDATE, phaseLevel = 3u)
    }

    @Test fun `build - respects configuration`() {
        val qualifier = with(StandardQualifierBuilder()) {
            phaseMilestone(17u)
            releaseMetadata = "FINAL"
            intermediateReleaseMetadata = "I"
            build()
        }
        qualifier.assert(MILESTONE, 17u, "FINAL", "I")
    }

    @Test fun `build - result is unique and independent`() {
        val builder = StandardQualifierBuilder()
        val qualifier1 = with(builder) {
            phaseMilestone(17u)
            releaseMetadata = "FINAL"
            intermediateReleaseMetadata = "I"
            build()
        }
        val qualifier2 = with(builder) {
            phaseRC(2u)
            releaseMetadata = ""
            build()
        }
        qualifier1.assert(MILESTONE, 17u, "FINAL", "I")
        qualifier2.assert(RELEASE_CANDIDATE, 2u)
    }

    @Test fun `of - standard usage`() {
        val qualifier = StandardQualifier.of {
            phaseBeta(3u)
            releaseMetadata = "MD"
        }
        qualifier.assert(BETA, 3u, "MD", "MD")
    }

    @Test fun `copyWith - standard usage`() {
        val original = StandardQualifier(ALPHA, 2u, "MD", "MD")
        val copy = original.copyWith {
            phaseLevel++
            finalReleaseMetadata = "ID-23"
        }
        original.assert(ALPHA, 2u, "MD", "MD")
        copy.assert(ALPHA, 3u, "ID-23", "MD")
    }

    @Test fun `version builder extension - qualifierOf`() {
        val version = StandardVersion.of<StandardQualifier> {
            qualifierOf {
                phaseAlpha(3u)
                finalReleaseMetadata = "MD"
            }
        }
        version.qualifier.assert(phase = ALPHA, phaseLevel = 3u, finalReleaseMetadata = "MD")
    }

    private fun StandardQualifier.assert(
        phase: Phase = RELEASE_CANDIDATE, phaseLevel: UInt = 1u,
        finalReleaseMetadata: String = "", intermediateReleaseMetadata: String = ""
    ) {
        assertEquals(phase, this.phase)
        assertEquals(phaseLevel, this.phaseLevel)
        assertEquals(finalReleaseMetadata, this.finalReleaseMetadata)
        assertEquals(intermediateReleaseMetadata, this.intermediateReleaseMetadata)
    }

    private fun StandardQualifierBuilder.assert(
        phase: Phase = RELEASE_CANDIDATE, phaseLevel: UInt = 1u,
        releaseMetadata: String = "",
        finalReleaseMetadata: String = "",
        intermediateReleaseMetadata: String = ""
    ) {
        assertEquals(phase, this.phase)
        assertEquals(phaseLevel, this.phaseLevel)
        assertEquals(releaseMetadata, this.releaseMetadata)
        assertEquals(finalReleaseMetadata, this.finalReleaseMetadata)
        assertEquals(intermediateReleaseMetadata, this.intermediateReleaseMetadata)
    }
}
