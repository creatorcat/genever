/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.core.reqtest

import de.creatorcat.genever.core.VersionGenerationSpecBuilder
import de.creatorcat.kotiya.matchers.common.isGreaterEqThan
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.pard.cova.annotation.VerifiesArtifact

/**
 * Version comparison test simulating the Eclipse versioning scheme.
 */
@ExperimentalUnsignedTypes
@VerifiesArtifact("reqFormatEclipse")
class EclipseStdVersionComparisonTest : StandardVersionComparisonTestBase<EclipseEquivalentVersion>() {
    override fun provideBasicGenerationSpec(): VersionGenerationSpecBuilder {
        return VersionGenerationSpecBuilder().apply {
            useEclipseStyle()
        }
    }

    override fun generateComparableVersion(versionString: String): EclipseEquivalentVersion {
        val parts = versionString.split('.')
        assertThat("version parts separated by '.'", parts.size, isGreaterEqThan(4))
        return EclipseEquivalentVersion(
            parts[0].toInt(), parts[1].toInt(), parts[2].toInt(),
            parts.drop(3).joinToString(separator = "")
        )
    }
}

@ExperimentalUnsignedTypes
class EclipseEquivalentVersion(
    private val major: Int, private val minor: Int, private val patch: Int,
    private val qualifier: String
) : Comparable<EclipseEquivalentVersion> {

    override fun compareTo(other: EclipseEquivalentVersion): Int {
        var result = this.major - other.major
        if (result == 0) result = this.minor - other.minor
        if (result == 0) result = this.patch - other.patch
        if (result == 0) result = this.qualifier.compareTo(other.qualifier)
        return result
    }

} 
