/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file. 
 */

package de.creatorcat.genever.core

import de.creatorcat.kotiya.matchers.and
import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.reflect.assertCompareToConsistencyWithEquals
import de.creatorcat.kotiya.test.core.reflect.assertCompareToContractFor
import de.creatorcat.kotiya.test.core.reflect.assertEqualsContractFor
import de.creatorcat.kotiya.test.core.reflect.assertHashCodeContractFor
import de.creatorcat.kotiya.test.core.stubbing.mustNotBeCalled
import de.creatorcat.pard.cova.annotation.VerifiesArtifact
import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

@ExperimentalUnsignedTypes
class StandardVersionTest {

    private val testObjects = listOf(
        version(23, 1), version(17, 1),
        version(17, 5, 0), version(17, 5), version(17, 5, 0),
        version(17, 2), version(17, 5), version(17, 9)
    )

    @Test fun `equals - functionality`() {
        assertEquals(version(17, 1, 2, 3), version(17, 1, 2, 3))
        assertEquals(version(17, 1), version(17, 1, 0))
        assertEquals(version(17, 1, 0), version(17, 1, 0, 0))
        assertEquals(version(17, 1, 0), version(17, 1))
        assertNotEquals(version(17, 1, 2), version(17, 1, 3))
        assertNotEquals(version(4, 1), version(5, 1))
        assertNotEquals(version(4, 1), version(4, 1, 0, 0, 1))
    }

    @Test fun `equals - general contract`() {
        StandardVersion<DummyQualifier>::equals.assertEqualsContractFor(testObjects)
    }

    @Test fun `hashCode - general contract`() {
        StandardVersion<DummyQualifier>::hashCode.assertHashCodeContractFor(testObjects)
    }

    @VerifiesArtifact("reqVersionComparison")
    @Test fun `compareTo - functionality`() {
        assertTrue(version(17, 1, 2) > version(17, 1, 1))
        assertTrue(version(17, 1, 2) < version(17, 1, 3))
        assertTrue(version(17, 1, 2) > version(16, 1, 2))
        assertTrue(version(17, 1, 2, 0, 1) > version(17, 1, 2, 0, 0, 1))
    }

    @VerifiesArtifact("reqVersionComparison")
    @Test fun `compareTo - general contract`() {
        StandardVersion<DummyQualifier>::compareTo.assertCompareToContractFor(testObjects)
        StandardVersion<DummyQualifier>::compareTo.assertCompareToConsistencyWithEquals(testObjects)
    }

    @Test fun `toString - contains base and qualifier`() {
        with(version(23, 1, 2, 3)) {
            assertThat(
                toString(),
                contains(qualifier.toString())
                    and contains(base.toString())
            )
        }
    }

    @Test fun `levelOfBaseComponent - is associated to base`() {
        val version = version(1, 4, 3, 2, 1)
        for (idx in 0u..3u) assertEquals(4u - idx, version.levelOfBaseComponent(idx))
        for (idx in 4u..7u) assertEquals(0u, version.levelOfBaseComponent(idx))
    }

    @Ignore // TODO enable once uint bug is fixed see https://youtrack.jetbrains.com/issue/KT-28855
    @VerifiesArtifact("reqVersionDef", "reqBaseVersionDef")
    @Test fun `construction - var args and list`() {
        // TODO since the ctor is now disabled due to kapt usage, this code must be disabled too
//        val vFromVarArgs = StandardVersion(DummyQualifier(1), 1u, 0u, 2u)
//        val vFromList = StandardVersion(DummyQualifier(), listOf(1u, 0u, 2u))
//        assertEquals(vFromList, vFromVarArgs)
    }

    private fun version(qualifierValue: Int, vararg baseComponents: Int) =
        StandardVersion(DummyQualifier(qualifierValue), baseComponents.map(Int::toUInt))

    private data class DummyQualifier(
        val value: Int = 1
    ) : VersionQualifier<DummyQualifier> {
        override fun generate(spec: VersionGenerationSpec) = mustNotBeCalled()
        override fun compareTo(other: DummyQualifier) = value.compareTo(other.value)
        override fun toString() = "Q($value)"
    }
}
