/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.core.reqtest

import de.creatorcat.genever.core.StandardQualifier
import de.creatorcat.genever.core.StandardVersionBuilder
import de.creatorcat.genever.core.qualifierOf

/**
 * A builder for the smallest version in all comparison test data.
 * This version must be less than all versions build by [getOrderedStandardVersionBuilders].
 *
 * @see StandardVersionComparisonTestBase
 */
@ExperimentalUnsignedTypes
fun getSmallestVersionBuilder(): StandardVersionBuilder<StandardQualifier> =
    StandardVersionBuilder<StandardQualifier>().apply {
        base.setAll(0u, 0u, 0u)
        qualifierOf { phaseAlpha(1u) }
    }

/**
 * A list of lists of version builders that produce ordered versions for comparison tests.
 * Each sub-list holds builders associated to the same final release i.e. they only differ
 * in their development phase.
 *
 * @see StandardVersionComparisonTestBase
 */
@ExperimentalUnsignedTypes
fun getOrderedStandardVersionBuilders(): List<List<StandardVersionBuilder<StandardQualifier>>> =
    listOf(
        vList({
            base.setAll(0u, 0u, 1u)
            qualifierOf { phaseAlpha(1u) }
        }, {
            base.setAll(0u, 0u, 1u)
            qualifierOf { phaseAlpha(2u); releaseMetadata = "123" }
        }, {
            base.setAll(0u, 0u, 1u)
            qualifierOf { phaseAlpha(23u) }
        }, {
            base.setAll(0u, 0u, 1u)
            qualifierOf { phaseBeta(1u); releaseMetadata = "MD" }
        }, {
            base.setAll(0u, 0u, 1u)
            qualifierOf { phaseMilestone(1u) }
        }, {
            base.setAll(0u, 0u, 1u)
            qualifierOf { phaseRC(1u) }
        }),
        vList({
            base.setAll(0u, 0u, 2u)
            qualifierOf { phaseAlpha(1u) }
        }),
        vList({
            base.setAll(0u, 0u, 3u)
            qualifierOf { phaseAlpha(1u); intermediateReleaseMetadata = "IMD" }
        }),
        vList({
            base.setAll(0u, 1u, 0u)
            qualifierOf { phaseAlpha(1u); finalReleaseMetadata = "FMD" }
        }),
        vList({
            base.setAll(0u, 2u, 0u)
            qualifierOf { phaseAlpha(1u) }
        }),
        vList({
            base.setAll(0u, 2u, 3u)
            qualifierOf { phaseRC(5u); releaseMetadata = "123" }
        }),
        vList({
            base.setAll(0u, 2u, 4u)
            qualifierOf { phaseRC(5u); intermediateReleaseMetadata = "0" }
        }),
        vList({
            base.setAll(0u, 3u, 0u)
            qualifierOf { phaseRC(5u) }
        }),
        vList({
            base.setAll(1u, 0u, 0u)
            qualifierOf { phaseRC(5u); releaseMetadata = "MD" }
        }),
        vList({
            base.setAll(1u, 1u, 1u)
            qualifierOf { phaseBeta(1u); releaseMetadata = "XXX" }
        }),
        vList({
            base.setAll(2u, 0u, 0u)
            qualifierOf { phaseBeta(1u) }
        }, {
            base.setAll(2u, 0u, 0u)
            qualifierOf { phaseBeta(8u) }
        }, {
            base.setAll(2u, 0u, 0u)
            qualifierOf { phaseBeta(10u) }
        })
    )

@ExperimentalUnsignedTypes
private fun vList(
    vararg builderConfigs: StandardVersionBuilder<StandardQualifier>.() -> Unit
): List<StandardVersionBuilder<StandardQualifier>> =
    builderConfigs.map {
        StandardVersionBuilder<StandardQualifier>().apply(it)
    }
