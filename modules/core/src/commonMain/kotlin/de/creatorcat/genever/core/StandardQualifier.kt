/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file. 
 */

package de.creatorcat.genever.core

import de.creatorcat.genever.core.ReleaseType.FINAL
import de.creatorcat.genever.core.ReleaseType.INTERMEDIATE
import de.creatorcat.genever.core.ReleaseType.SNAPSHOT
import de.creatorcat.genever.core.StandardQualifier.Phase.ALPHA
import de.creatorcat.genever.core.StandardQualifier.Phase.BETA
import de.creatorcat.genever.core.StandardQualifier.Phase.MILESTONE
import de.creatorcat.genever.core.StandardQualifier.Phase.RELEASE_CANDIDATE
import de.creatorcat.kotiya.core.math.pow
import de.creatorcat.kotiya.core.text.orEmptyIf
import de.creatorcat.kotiya.matchers.common.isLessThan
import de.creatorcat.kotiya.matchers.require
import de.creatorcat.pard.cova.annotation.ImplementsArtifact

/**
 * Implementation of an immutable [VersionQualifier] that is compatible with all major standard approaches:
 * Maven, Gradle, Eclipse and even MS-Build numeric style is supported.
 * The generated qualifier consists of two parts separated by a `"-"` e.g. "alpha2-r2743", "beta3-20181123":
 * * *phase-part*: represents the state of development of the current release
 * * *meta-data*: gives some (optional) further information about the artifact (revision number, build-id etc.)
 *
 * The *meta-data* part will be set to `"SNAPSHOT"` for [SNAPSHOT][ReleaseType.SNAPSHOT] releases.
 *
 * *Note*: for this class [equality][equals] is not equivalent to [comparison][compareTo]!
 * The comparison only provides a minimum consistency with the [equals] method:
 * `(x < y || x > y) implies x != y`.
 *
 * ### Qualifier comparison
 *
 * While integer base version components are common in all standard version schemes,
 * the qualifier is handled differently in each framework.
 * GeneVer tries to ensure properly working versions for all supported schemes
 * by enforcing the respective qualifier style.
 *
 * Eclipse for example compares qualifiers lexicographically.
 * To ensure an intermediate release is considered newer than the respective snapshot,
 * GeneVer will automatically append release metadata if none is defined.
 * Also the phase level will be padded with zeros from '3' to '003'.
 *
 * But even in flexible systems as Maven or Gradle it is sometimes necessary
 * to prepend the given metadata with a 'v' to ensure it is considered newer
 * (lexicographically) than the string 'SNAPSHOT'.
 *
 * ### Numeric style generation
 *
 * To support MS-Build 4 part numeric versions, the qualifier can be generated as number by setting
 * [qualifierAsNumber][VersionGenerationSpec.qualifierAsNumber] to `true`.
 * The resulting number must be less than `2^16 - 1` according to the MS rules.
 * * The [phase] will be converted to an integer according to its order number (e.g. `"alpha"` to `0` etc.)
 * and defines the first digit (from left to right).
 * * The [phaseLevel] is used directly as the second digit.
 * * The *meta-data* number is parsed from the *meta-data* string to make the remaining digits.
 *   It is set to `0` for [SNAPSHOT][ReleaseType.SNAPSHOT] releases.
 *   Note, that any given meta-data must be [convertible to an unsigned integer][String.toUInt] or the
 *   generation will fail with an exception.
 *
 * *Example*: "alpha3-234" transforms to number `03234`, "beta2-SNAPSHOT" to `12000`
 *
 * @constructor Creates a qualifier with the given properties.
 * @param phase Current development phase of the software artifact to be versioned.
 *   Defaults to [RELEASE_CANDIDATE].
 *   The `phase` is always linked to the [phaseLevel]. Only together they form the *phase-part*
 *   of the version string, e.g. `"alpha3"`.
 * @param phaseLevel Level counter for the current development [phase]. Defaults to `1`.
 *   There are often multiple releases in each phase, e.g. `"alpha1"`, `"alpha2"`, `"beta1"` etc.
 *   *Note*: if [qualifierAsNumber][VersionGenerationSpec.qualifierAsNumber]
 *   or [lexicographicalQualifier][VersionGenerationSpec.lexicographicalQualifier] is specified on generation,
 *   the phase level is limited to 1 or 3 respectively digits.
 * @param finalReleaseMetadata Optional *meta-data* string to be attached to
 *   [final release][ReleaseType.FINAL] qualifiers. Empty by default.
 *   This is the place to put build- or revision-numbers or similar information, e.g. like in `"1.0.0-7345b"`.
 *   If this property is not empty, it might be necessary to prepend the phase name `"release"`
 *   before the metadata to assure proper version comparison.
 * @param intermediateReleaseMetadata Optional *meta-data* string to be attached to
 *   [intermediate release][ReleaseType.INTERMEDIATE] qualifiers. Empty by default.
 *   This is the place to put build- or revision-numbers or similar information,
 *   e.g. like in `"1.0.0-beta3-7345b"`.
 *   If this property is not empty, it might be necessary to prepend the prefix `"v"`
 *   before the metadata to assure proper version comparison.
 */
@ExperimentalUnsignedTypes
data class StandardQualifier(
    @ImplementsArtifact("reqQualifierPhase")
    val phase: Phase = RELEASE_CANDIDATE,
    @ImplementsArtifact("reqQualifierPhase")
    val phaseLevel: UInt = 1u,
    @ImplementsArtifact("reqQualifierMetaData")
    val finalReleaseMetadata: String = "",
    @ImplementsArtifact("reqQualifierMetaData")
    val intermediateReleaseMetadata: String = ""
) : VersionQualifier<StandardQualifier> {

    @ImplementsArtifact("reqFormatDefault", "reqFormatEclipse")
    override fun generate(spec: VersionGenerationSpec): String {
        if (spec.qualifierAsNumber) return generateAsNumber(spec)

        val phasePart = generatePhasePart(spec)
        val metaData = generateMetaData(spec)
        var qualifier = when {
            metaData.isBlank() -> phasePart
            phasePart.isBlank() -> metaData
            else -> "$phasePart$metaDataSeparator$metaData"
        }
        if (spec.upCaseCharacters) qualifier = qualifier.toUpperCase()
        return qualifier
    }

    @ImplementsArtifact("reqFormatDefault", "reqFormatEclipse")
    private fun generateMetaData(spec: VersionGenerationSpec): String =
        when (spec.releaseType) {
            FINAL -> finalReleaseMetadata
            INTERMEDIATE -> generateComparisonSafeIntermediateMetadata(spec)
            SNAPSHOT -> snapshotMetadata
        }

    private fun generateComparisonSafeIntermediateMetadata(spec: VersionGenerationSpec): String {
        val effectiveMetadata =
            if (intermediateReleaseMetadata.isBlank() && spec.lexicographicalQualifier)
                intermediateLexicoDefaultMetadata
            else
                intermediateReleaseMetadata
        val needsPrefix =
            effectiveMetadata.isNotBlank()
                && (spec.lexicographicalQualifier || !(effectiveMetadata.matches(Regex("\\d.*"))))
                && snapshotMetadata >= effectiveMetadata.take(snapshotMetadata.length)

        return if (needsPrefix) "v$effectiveMetadata" else effectiveMetadata
    }

    @ImplementsArtifact("reqFormatDefault", "reqFormatEclipse")
    private fun generatePhasePart(spec: VersionGenerationSpec): String =
        if (spec.releaseType == FINAL) {
            finalReleasePhaseName.orEmptyIf(
                !spec.forceNonEmptyQualifier
                    && (finalReleaseMetadata.isBlank()
                    || (!spec.lexicographicalQualifier && finalReleaseMetadata.matches(Regex("\\d.*")))
                    )
            )
        } else
            "${generatePhaseName(spec)}${generatePhaseLevel(spec)}"

    @ImplementsArtifact("reqFormatDefault", "reqFormatEclipse")
    private fun generatePhaseName(spec: VersionGenerationSpec): String =
        if (spec.preferAbbreviations) phase.abbreviation else phase.displayName

    @ImplementsArtifact("reqFormatEclipse")
    private fun generatePhaseLevel(spec: VersionGenerationSpec): String =
        if (spec.lexicographicalQualifier) {
            require(
                "phaseLevel of lexicographic qualifier", phaseLevel,
                isLessThan(10u pow lexicographicalSafetyPaddingSize)
            )
            phaseLevel.toString().padStart(lexicographicalSafetyPaddingSize.toInt(), '0')
        } else
            phaseLevel.toString()

    private fun generateAsNumber(spec: VersionGenerationSpec): String {
        // remember: number must be < 2^16 - 1 for MS-Assembly
        val phaseNum = if (spec.releaseType == FINAL) maxPhaseNumber else phase.ordinal
        require(
            "phaseLevel of numeric qualifier", phaseLevel,
            isLessThan(10u pow numericMaxPhaseLevelSize)
        )
        return "$phaseNum$phaseLevel${generateNumericMetaData(spec)}"
    }

    private fun generateNumericMetaData(spec: VersionGenerationSpec): String {
        val metaDataNumber: UInt
        try {
            metaDataNumber = when (spec.releaseType) {
                SNAPSHOT -> 0u
                INTERMEDIATE ->
                    if (intermediateReleaseMetadata.isNotBlank()) {
                        val parsedNum = intermediateReleaseMetadata.toUInt()
                        if (parsedNum == 0u) intermediateNumericDefaultMetadata else parsedNum
                    } else
                        intermediateNumericDefaultMetadata
                FINAL ->
                    if (finalReleaseMetadata.isNotBlank()) finalReleaseMetadata.toUInt()
                    else 0u
            }
        } catch (e: NumberFormatException) {
            throw IllegalArgumentException("release meta data cannot be parsed as unsigned integer")
        }
        require(
            "meta data of numeric qualifier", metaDataNumber,
            isLessThan(10u pow numericMetaDataPaddingSize)
        )
        return metaDataNumber.toString().padStart(numericMetaDataPaddingSize.toInt(), '0')
    }

    /**
     * Qualifiers of this type are compared by [phase] and [phaseLevel] only.
     * As the *meta-data* part is considered additional information
     * it should not play any role when comparing version qualifiers.
     *
     * First, the [phase] is compared according to its order.
     * If both phases match, the [phaseLevel] is compared numerically.
     *
     * Note, that this comparison only provides a minimum consistency with the [equals] method:
     * `(x < y || x > y) implies x != y`.
     */
    @ImplementsArtifact("reqQualifierComparison")
    override fun compareTo(other: StandardQualifier): Int {
        val phaseDiff = phase.compareTo(other.phase)
        return if (phaseDiff != 0) phaseDiff else phaseLevel.compareTo(other.phaseLevel)
    }

    companion object {
        // value of 3 ensures 999 releases which should be more than enough...
        @ImplementsArtifact("reqFormatEclipse")
        private const val lexicographicalSafetyPaddingSize: UInt = 3u
        // remember: only "release" will be greater "rc" in gradle-comparator
        private const val finalReleasePhaseName: String = "release"
        @ImplementsArtifact("reqFormatDefault")
        private const val metaDataSeparator: String = "-"
        private const val numericMetaDataPaddingSize: UInt = 3u
        private const val numericMaxPhaseLevelSize: UInt = 1u
        private const val snapshotMetadata = "SNAPSHOT"
        private const val intermediateLexicoDefaultMetadata = "Final"
        internal const val intermediateNumericDefaultMetadata = 100u
        /** Maximum number of possible [Phase] elements.
         * Value = 6 since qualifier as number must be < 2^16 - 1 for MS-Assembly */
        internal const val maxPhaseNumber: UInt = 6u
    }

    /**
     * Current development phase of the software artifact to be versioned.
     * Supports the following most common phases that are also supported in Maven version comparison.
     * Phases are ordered for [comparison][StandardQualifier.compareTo] in the order they appear here.
     * * [ALPHA]: "alpha" ([displayName]) - "a" ([abbreviation])
     * * [BETA]: "beta" - "b"
     * * [MILESTONE]: "milestone" - "m"
     * * [RELEASE_CANDIDATE]: "rc" - "rc"
     *
     * @param displayName human readable name of the phase that is used in the version string
     *   if [preferAbbreviations][VersionGenerationSpec.preferAbbreviations] is *not* set, e.g. `"alpha"`
     * @param abbreviation short abbreviation of the phase, e.g. `"a"` for `"alpha"`
     *   if [preferAbbreviations][VersionGenerationSpec.preferAbbreviations] is set
     */
    @ImplementsArtifact("reqQualifierPhase", "reqQualifierComparison")
    enum class Phase(
        val displayName: String,
        @ImplementsArtifact("reqFormatDefault", "reqFormatEclipse")
        val abbreviation: String
    ) {
        // remember: definition order == compare order
        /** The alpha development phase: [displayName] = "alpha", [abbreviation] = "a" */
        ALPHA("alpha", "a"),
        /** The beta development phase: [displayName] = "beta", [abbreviation] = "b" */
        BETA("beta", "b"),
        /** Development phase for reaching a certain milestone:
         * [displayName] = "milestone", [abbreviation] = "m" */
        MILESTONE("milestone", "m"),
        /** Late development phase short before the final release:
         * [displayName] = "rc", [abbreviation] = "rc" */
        RELEASE_CANDIDATE("rc", "rc")
        // there must never be more than maxPhaseNumber phases or the numerical qualifier will be to big
    }
}
