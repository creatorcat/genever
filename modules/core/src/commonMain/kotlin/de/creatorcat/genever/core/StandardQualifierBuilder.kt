/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.core

import de.creatorcat.genever.core.StandardQualifier.Phase
import de.creatorcat.genever.core.StandardQualifier.Phase.ALPHA
import de.creatorcat.genever.core.StandardQualifier.Phase.BETA
import de.creatorcat.genever.core.StandardQualifier.Phase.MILESTONE
import de.creatorcat.genever.core.StandardQualifier.Phase.RELEASE_CANDIDATE
import de.creatorcat.pard.cova.annotation.ImplementsArtifact

/**
 * Builder for producing [StandardQualifier] objects.
 * The builder provides convenience variables to configure all properties
 * and additional functions to simplify configuration.
 *
 * Instead of instantiating a builder directly use one of the convenience methods listed below.
 *
 * @see of
 * @see copyWith
 */
@ExperimentalUnsignedTypes
open class StandardQualifierBuilder {

    /**
     * Use this to define the [StandardQualifier.phase] of the qualifier to be [build].
     * Note, that the phase and its level may also be defined at once via one of the shortcuts
     * [phaseAlpha], [phaseBeta], [phaseMilestone] and [phaseRC].
     *
     * Defaults to [RELEASE_CANDIDATE].
     */
    @ImplementsArtifact("reqQualifierPhase")
    open var phase: Phase = RELEASE_CANDIDATE
    /**
     * Use this to define the [StandardQualifier.phaseLevel] of the qualifier to be [build].
     * Note, that the phase and its level may also be defined at once via one of the shortcuts
     * [phaseAlpha], [phaseBeta], [phaseMilestone] and [phaseRC].
     *
     * Defaults to `1u`.
     */
    @ImplementsArtifact("reqQualifierPhase")
    open var phaseLevel: UInt = 1u

    /**
     * Shortcut to define [phase] to be [ALPHA] and [phaseLevel] to [level] (defaults to `1u`).
     */
    fun phaseAlpha(level: UInt = 1u) {
        phase = ALPHA
        phaseLevel = level
    }

    /**
     * Shortcut to define [phase] to be [BETA] and [phaseLevel] to [level] (defaults to `1u`).
     */
    fun phaseBeta(level: UInt = 1u) {
        phase = BETA
        phaseLevel = level
    }

    /**
     * Shortcut to define [phase] to be [MILESTONE] and [phaseLevel] to [level] (defaults to `1u`).
     */
    fun phaseMilestone(level: UInt = 1u) {
        phase = MILESTONE
        phaseLevel = level
    }

    /**
     * Shortcut to define [phase] to be [RELEASE_CANDIDATE] and [phaseLevel] to [level] (defaults to `1u`).
     */
    fun phaseRC(level: UInt = 1u) {
        phase = RELEASE_CANDIDATE
        phaseLevel = level
    }

    /**
     * Use this to define [finalReleaseMetadata] and [intermediateReleaseMetadata] to the same value.
     *
     * Defaults to the empty string.
     */
    open var releaseMetadata: String = ""
        set(value) {
            field = value
            finalReleaseMetadata = value
            intermediateReleaseMetadata = value
        }

    /**
     * Use this to define the [StandardQualifier.finalReleaseMetadata] of the qualifier to be [build].
     * To define final and intermediate release metadata at once use [releaseMetadata].
     *
     * Defaults to the empty string.
     */
    @ImplementsArtifact("reqQualifierMetaData")
    open var finalReleaseMetadata: String = ""
    /**
     * Use this to define the [StandardQualifier.intermediateReleaseMetadata] of the qualifier to be [build].
     * To define final and intermediate release metadata at once use [releaseMetadata].
     *
     * Defaults to the empty string.
     */
    @ImplementsArtifact("reqQualifierMetaData")
    open var intermediateReleaseMetadata: String = ""

    /**
     * Build the qualifier based on the configured properties.
     * Each result will be unique and independent of further changes to the builder.
     */
    open fun build(): StandardQualifier =
        StandardQualifier(phase, phaseLevel, finalReleaseMetadata, intermediateReleaseMetadata)
}

/**
 * Creates a [StandardQualifier] based on the given [configuration] block
 * that is applied on a [StandardQualifierBuilder].
 *
 * Example:
 *
 *     val myQualifier = StandardQualifier.of {
 *         phaseAlpha(2u)
 *         releaseMetadata = "rev123"
 *     }
 */
@ExperimentalUnsignedTypes
fun StandardQualifier.Companion.of(
    configuration: StandardQualifierBuilder.() -> Unit
): StandardQualifier =
    with(StandardQualifierBuilder()) {
        configuration()
        build()
    }

/**
 * Creates a copy of this qualifier but with applying the [adjustment] block on a [StandardQualifierBuilder].
 * The builder will be initialized with the values of this qualifier.
 *
 * Example:
 *
 *     val otherQualifier = myQualifier.copyWith {
 *         phaseLevel++
 *         finalReleaseMetadata = "BUILD-ID-23"
 *     }
 */
@ExperimentalUnsignedTypes
fun StandardQualifier.copyWith(
    adjustment: StandardQualifierBuilder.() -> Unit
): StandardQualifier {
    val template = this
    return with(StandardQualifierBuilder()) {
        phase = template.phase
        phaseLevel = template.phaseLevel
        intermediateReleaseMetadata = template.intermediateReleaseMetadata
        finalReleaseMetadata = template.finalReleaseMetadata
        adjustment()
        build()
    }
}

/**
 * Sets the [qualifier][GenericVersionBuilder.qualifier] to a new [StandardQualifier]
 * created by a [StandardQualifierBuilder] with applied [configuration].
 */
@ExperimentalUnsignedTypes
fun GenericVersionBuilder<StandardQualifier, *>.qualifierOf(
    configuration: StandardQualifierBuilder.() -> Unit
) {
    qualifier = StandardQualifier.of(configuration)
}
