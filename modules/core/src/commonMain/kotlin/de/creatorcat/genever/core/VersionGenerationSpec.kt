/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file. 
 */

package de.creatorcat.genever.core

import de.creatorcat.pard.cova.annotation.ImplementsArtifact

/**
 * Specifies how the version string shall be [generated][GenericVersion.generate].
 * See the doc of each property for more information.
 * Use a [builder][VersionGenerationSpecBuilder] to safely create a spec.
 */
@ImplementsArtifact("reqVersionGeneration")
interface VersionGenerationSpec {
    /**
     * Type of the release whose version shall be generated.
     * This may influence [qualifier generation][VersionQualifier.generate].
     */
    val releaseType: ReleaseType
    /** Separator between [base-version][GenericVersion.base] and [qualifier][GenericVersion.qualifier]. */
    @ImplementsArtifact("reqFormatDefault", "reqFormatEclipse")
    val qualifierSeparator: String
    /**
     * If `true` the generated qualifier shall never be empty and shall be filled with some
     * default value if not specified otherwise.
     */
    @ImplementsArtifact("reqFormatEclipse")
    val forceNonEmptyQualifier: Boolean
    /**
     * If `true` the qualifier shall be generated as a number (to support e.g. MS-Net assembly versions).
     * How this is done and if it is even possible depends on the actually used [VersionQualifier].
     * Qualifier implementations should throw an [IllegalArgumentException]
     * if it is not possible with current data or not supported at all.
     */
    val qualifierAsNumber: Boolean
    /**
     * If `true` the generated qualifier shall comply with the lexicographical order (to support e.g. Eclipse).
     * How this is done and if it is even possible depends on the actually used [VersionQualifier].
     * Qualifier implementations should throw an [IllegalArgumentException]
     * if it is not possible with current data or not supported at all.
     */
    @ImplementsArtifact("reqFormatEclipse")
    val lexicographicalQualifier: Boolean
    /**
     * If `true` the generated qualifier shall use abbreviations where appropriate e.g. for phase names (`alpha -> a`).
     * How this is done and if it is even possible depends on the actually used [VersionQualifier].
     * Since it's only named *prefer*, qualifier implementations should ignore this flag if they cannot support it.
     */
    @ImplementsArtifact("reqFormatDefault", "reqFormatEclipse")
    val preferAbbreviations: Boolean
    /**
     * If `true` the generated version shall have the 3 standard [base][GenericVersion.base] components
     * (major.minor.patch).
     *
     * If not enough components are defined, the missing parts will be set to zero
     * (aligned to [GenericVersion.levelOfBaseComponent]).
     * The zeros add no further value to the version and are thus safe to add.
     *
     * If there are more than 3 components the generation method should throw an [IllegalArgumentException].
     * Cutting those parts would probably remove important information that uniquely identifies the version.
     */
    @ImplementsArtifact("reqFormatEclipse")
    val ensure3ComponentBase: Boolean
    /**
     * If `true` all characters in the generated version qualifier shall be in upper case.
     */
    val upCaseCharacters: Boolean

    /** The companion provides static creation methods. */
    companion object
}

/**
 * Type of the software release whose version string shall be generated.
 *
 * @see VersionGenerationSpec
 * @see GenericVersion.generate
 */
enum class ReleaseType {
    /** A snapshot release, e.g. 1.2.3-alpha4-SNAPSHOT. */
    SNAPSHOT,
    /** An intermediate release like alpha, milestone or release candidate (rc), e.g. 1.2.3-alpha4 */
    INTERMEDIATE,
    /** The final or actual release for the associated base version, e.g. 1.2.3. */
    FINAL
}
