/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.core

import de.creatorcat.kotiya.core.structures.setAll
import de.creatorcat.pard.cova.annotation.ImplementsArtifact

/**
 * A builder for [StandardVersion] objects.
 * Instead of instantiating a builder directly use one of the convenience methods listed below.
 *
 * @see of
 * @see copyWith
 */
@ExperimentalUnsignedTypes
@ImplementsArtifact("reqVersionDef")
open class StandardVersionBuilder<Q : VersionQualifier<Q>> : GenericVersionBuilder<Q, StandardVersion<Q>>() {

    override fun build(): StandardVersion<Q> =
        StandardVersion(
            checkNotNull(qualifier) { "no qualifier defined" },
            base.components
        )
}

/**
 * Creates a [StandardVersion] based on the given [configuration] block that is applied on a [StandardVersionBuilder].
 *
 * Example:
 *
 *     val myVersion = StandardVersion.of {
 *         majorLevel = 2u
 *         patchLevel = 1u
 *         qualifier = myQualifier
 *     }
 */
@ExperimentalUnsignedTypes
fun <Q : VersionQualifier<Q>> StandardVersion.Companion.of(
    configuration: StandardVersionBuilder<Q>.() -> Unit
): StandardVersion<Q> =
    with(StandardVersionBuilder<Q>()) {
        configuration()
        build()
    }

/**
 * Creates a copy of this version but with applying the [adjustment] block on a [StandardVersionBuilder].
 * The builder will be initialized with the values of this version.
 *
 * Example:
 *
 *     val otherVersion = myVersion.copyWith {
 *         majorLevel = 2u
 *         patchLevel = 1u
 *     }
 */
@ExperimentalUnsignedTypes
fun <Q : VersionQualifier<Q>> StandardVersion<Q>.copyWith(
    adjustment: StandardVersionBuilder<Q>.() -> Unit
): StandardVersion<Q> {
    val templateVersion = this
    return with(StandardVersionBuilder<Q>()) {
        qualifier = templateVersion.qualifier
        base.components.setAll(templateVersion.base)
        adjustment()
        build()
    }
}
