/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file. 
 */

package de.creatorcat.genever.core

import de.creatorcat.kotiya.core.structures.get
import de.creatorcat.kotiya.core.structures.uSize
import de.creatorcat.kotiya.matchers.common.isLessEqThan
import de.creatorcat.kotiya.matchers.require
import de.creatorcat.pard.cova.annotation.ImplementsArtifact

/**
 * Representation of a software version that is meant to support many common version styles (hence: *generic*).
 * It only assumes the following properties for every version:
 * * it has a [base] consisting of a list of positive integers (e.g. `1.2.3`)
 * * it has an arbitrary [qualifier] of type [Q]
 * * it is [Comparable] to other versions with same qualifier type [Q]
 *   (and has a proper [equals] method)
 * * its final purpose is to [generate] a version string (e.g. for naming release artifacts)
 *
 * Note that the [base]-version is always comparable ([compareBase])
 * and can be generated ([generateBase]), regardless of the qualifier type.
 */
@ExperimentalUnsignedTypes
interface GenericVersion<Q : VersionQualifier<Q>> : Comparable<GenericVersion<Q>> {

    /**
     * The base version consists of an arbitrary number of positive integer components (even none is allowed).
     * This is the list of base components that are actually defined
     * and only these should (usually) be considered when [generating the version string][generateBase].
     *
     * However, the numeric value of a component (called its "level") is defined for components
     * of any index greater than 0 by setting `level == 0` for components not defined in [base].
     * The level of any index may be retrieved via [levelOfBaseComponent].
     * So for the base-version `1` the level of component `0` is `1`,
     * the level of component `1, 2, ...` is `0`.
     * This way the base version `1` and `1.0.0` can be considered equal.
     * There are also some named components (aligned to SemVer): [majorLevel], [minorLevel] and [patchLevel].
     */
    @ImplementsArtifact("reqVersionDef", "reqBaseVersionDef")
    val base: List<UInt>

    /**
     * The version qualifier.
     */
    @ImplementsArtifact("reqVersionDef")
    val qualifier: Q

    /**
     * To compare versions they need to have the same qualifier type [Q].
     * Firstly the [base]-version is compared via [compareBase].
     * If both have equal base-versions the final result is the qualifier comparison result.
     */
    @ImplementsArtifact("reqVersionComparison")
    override fun compareTo(other: GenericVersion<Q>): Int {
        if (other === this) return 0
        val baseDiff = compareBase(other)
        return if (baseDiff != 0) baseDiff else qualifier.compareTo(other.qualifier)
    }

    /**
     * Generates a string representation of this version in the format
     * `<base><separator><qualifier>` or `<base>` if `<qualifier>` is blank with:
     * * `<base>` being the result of [generateBase] (called with [spec])
     * * `<separator>` being [VersionGenerationSpec.qualifierSeparator] of the [spec]
     * * `<qualifier>` being the result of [VersionQualifier.generate] (called with [spec])
     */
    @ImplementsArtifact("reqVersionGeneration", "reqFormatDefault", "reqFormatEclipse")
    fun generate(spec: VersionGenerationSpec): String {
        val qualifierPart = qualifier.generate(spec)
        val basePart = generateBase(spec)
        return if (qualifierPart.isNotBlank())
            "$basePart${spec.qualifierSeparator}$qualifierPart"
        else
            basePart
    }

    /**
     * The equals method of a [GenericVersion] must provide at least a minimum consistency
     * with the [compareTo] method: `(x < y || x > y) implies x != y`
     * Of course the [general equals contract][Any.equals] also must be fulfilled.
     */
    override fun equals(other: Any?): Boolean

    override fun hashCode(): Int

    /**
     * Retrieve the level of the major component (component `0`)
     * when interpreting the [base-version][base] as `major.minor.patch.others...`.
     * Shortcut to [levelOfBaseComponent(0u)][levelOfBaseComponent].
     */
    val majorLevel: UInt
        get() = levelOfBaseComponent(0u)

    /**
     * Retrieve the level of the minor component (component `1`)
     * when interpreting the [base-version][base] as `major.minor.patch.others...`.
     * Shortcut to [levelOfBaseComponent(1u)][levelOfBaseComponent].
     */
    val minorLevel: UInt
        get() = levelOfBaseComponent(1u)

    /**
     * Retrieve the level of the patch component (component `2`)
     * when interpreting the [base-version][base] as `major.minor.patch.others...`.
     * Shortcut to [levelOfBaseComponent(2u)][levelOfBaseComponent].
     */
    val patchLevel: UInt
        get() = levelOfBaseComponent(2u)

    /**
     * Retrieve the level of a [base-version][base] component at [index], whether it is actually defined or not.
     * Undefined components are considered as having `level == 0`.
     *
     * There are also some named components (aligned to SemVer): [majorLevel], [minorLevel] and [patchLevel].
     */
    fun levelOfBaseComponent(index: UInt): UInt {
        return if (base.uSize > index) base[index] else 0u
    }
}

/**
 * Generates a string representation of the [base-version][GenericVersion.base]
 * by separating the version components with dots (e.g. 1.2.3).
 *
 * If [VersionGenerationSpec.ensure3ComponentBase] is set in the [spec],
 * the first 3 components retrieved via [levelOfBaseComponent][GenericVersion.levelOfBaseComponent] will be used
 * unless the base-version has more than 3 components in which case an [IllegalArgumentException] is thrown.
 *
 * If the base-version is empty, at least one component will be used to create the string `"0"`.
 */
@ExperimentalUnsignedTypes
@ImplementsArtifact("reqFormatDefault", "reqFormatEclipse")
fun GenericVersion<*>.generateBase(spec: VersionGenerationSpec): String {
    val lastIncludedComponent: UInt = if (spec.ensure3ComponentBase) {
        require("number of base version components", base.size, isLessEqThan(3))
        2u
    } else {
        // always generate at least one component
        if (base.isEmpty()) 0u else base.uSize - 1u
    }
    return (0u..lastIncludedComponent)
        .map { levelOfBaseComponent(it) }
        .joinToString(".")
}

/**
 * Compare the [base-version][GenericVersion.base] by comparing the
 * [levelOfBaseComponent][GenericVersion.levelOfBaseComponent]
 * for each component index present in this or the [other] version.
 *
 * Note that missing components are considered to be `0` so `1` is equal to `1.0.0`.
 */
@ExperimentalUnsignedTypes
@ImplementsArtifact("reqVersionComparison", "reqBaseVersionComparison")
fun GenericVersion<*>.compareBase(other: GenericVersion<*>): Int {
    for (i in 0u..maxOf(base.uSize, other.base.uSize)) {
        val diff = levelOfBaseComponent(i).compareTo(other.levelOfBaseComponent(i))
        if (diff != 0) return diff
    }
    return 0
}

/**
 * Represents the part of a software version that is usually called the qualifier
 * and is placed at the end of the version string.
 *
 * A qualifier usually provides additional information about the released artifact e.g.
 * the development phase (alpha, beta...) or some sort of revision or build number.
 * However, this interface only assumes the following properties for every implementation:
 * * it is [Comparable] to other qualifiers of the same type
 *   (and has a proper [equals] method)
 * * a qualifier string can be [generated][generate]
 */
interface VersionQualifier<in Q : VersionQualifier<Q>> : Comparable<Q> {

    /**
     * Generates a string representation of the qualifier with regards to the [spec].
     */
    fun generate(spec: VersionGenerationSpec): String

    /**
     * The comparison method of a [VersionQualifier] must provide at least a minimum consistency
     * with the [equals] method: `(x < y || x > y) implies x != y`.
     */
    @ImplementsArtifact("reqVersionComparison", "reqQualifierComparison")
    override fun compareTo(other: Q): Int

    /**
     * The equals method of a [VersionQualifier] must provide at least a minimum consistency
     * with the [compareTo] method: `(x < y || x > y) implies x != y`
     * Of course the [general equals contract][Any.equals] also must be fulfilled.
     */
    override fun equals(other: Any?): Boolean

    override fun hashCode(): Int
}
