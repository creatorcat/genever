/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.core

import de.creatorcat.kotiya.core.reflect.otherProperty
import de.creatorcat.kotiya.core.structures.get
import de.creatorcat.kotiya.core.structures.set
import de.creatorcat.kotiya.core.structures.setAll
import de.creatorcat.kotiya.core.structures.uSize
import de.creatorcat.pard.cova.annotation.ImplementsArtifact

/**
 * Base class for version builders producing [V]-versions with [Q]-qualifiers.
 * The builder provides convenience variables to configure all properties of a [GenericVersion].
 *
 * Example:
 *
 *     val myVersion = with(builder) {
 *         majorLevel = 2u
 *         qualifier = myQualifier
 *         build()
 *     }
 */
@ExperimentalUnsignedTypes
abstract class GenericVersionBuilder<Q : VersionQualifier<Q>, V : GenericVersion<Q>> {

    /**
     * The [base components of the version][GenericVersion.base] to be [build].
     * Use indexed access operator `[ ]` to define base components of any index (gaps will be closed by zeros)
     * or [setAll][MutableBaseComponents.setAll] to define all at once.
     *
     * Defaults to be empty (no components).
     *
     * Note, that although base components may be defined for any index,
     * retrieving an undefined component will lead to an [IndexOutOfBoundsException].
     */
    @ImplementsArtifact("reqVersionDef")
    open val base: MutableBaseComponents = MutableBaseComponents()

    /**
     * Use this to define the [level of the major base component][GenericVersion.majorLevel]
     * of the version to be [build].
     * Shortcut to `base[0u]`.
     *
     * @see base
     */
    var majorLevel: UInt
        get() = base[0u]
        set(value) {
            base[0u] = value
        }

    /**
     * Use this to define the [level of the minor base component][GenericVersion.minorLevel]
     * of the version to be [build].
     * Shortcut to `base[1u]`.
     *
     * @see base
     */
    var minorLevel: UInt
        get() = base[1u]
        set(value) {
            base[1u] = value
        }

    /**
     * Use this to define the [level of the patch base component][GenericVersion.patchLevel]
     * of the version to be [build].
     * Shortcut to `base[2u]`.
     *
     * @see base
     */
    var patchLevel: UInt
        get() = base[2u]
        set(value) {
            base[2u] = value
        }

    /**
     * The [qualifier of the version][GenericVersion.qualifier] to be [build].
     * Defaults to `null` and must always be specified.
     */
    @ImplementsArtifact("reqVersionDef")
    open var qualifier: Q? = null

    /**
     * Build the version object based on the configured properties.
     * Each result will be unique and independent of further changes to the builder.
     */
    abstract fun build(): V
}

/**
 * Provides indexed access to the components of the [base version][GenericVersionBuilder.base]
 * that shall be build by the associated [GenericVersionBuilder] via the `[ ]` operator.
 *
 * Components may be defined for any index, thus extending the [components] list.
 * Since there must be no "gaps" inside a base version definition,
 * all undefined components between the last defined and the given index will be defined to `0`.
 * For example, if `base == 1.0` then `levelOfBaseComponent[4] = 2` will produce `base == 1.0.0.0.2`.
 *
 * However, retrieving an undefined component will lead to an [IndexOutOfBoundsException].
 */
@ExperimentalUnsignedTypes
@ImplementsArtifact("reqBaseVersionDef")
open class MutableBaseComponents {

    /**
     * Actual backing list used when reading or writing components via any of the provided methods.
     */
    protected val internalComponents: MutableList<UInt> = ArrayList(3) // assume major.minor.patch default

    /**
     * The list of currently defined components.
     * This may be used for advanced manipulation of the components.
     */
    // TODO design
    // This is separated from internalComponents to allow subclasses to modify the public visible components list
    open val components: MutableList<UInt> by otherProperty(::internalComponents)

    /**
     * Indexed access operator to define base components.
     * If [index] is above the current size, missing components will be filled up with zeros.
     */
    open operator fun set(index: UInt, value: UInt) {
        while (internalComponents.uSize <= index) internalComponents.add(0u)
        internalComponents[index] = value
    }

    /**
     * Indexed access operator to read base components.
     * Unlike the [set]-operator, retrieving an undefined component will lead to an [IndexOutOfBoundsException].
     */
    open operator fun get(index: UInt): UInt = internalComponents[index]

    /**
     * Replace all current components by the [newComponents].
     */
    open fun setAll(vararg newComponents: UInt): Unit = internalComponents.setAll(newComponents)

    override fun toString(): String = "BaseComponents(${internalComponents})"
}
