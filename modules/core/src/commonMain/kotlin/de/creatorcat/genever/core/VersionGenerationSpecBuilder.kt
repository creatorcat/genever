/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file. 
 */

package de.creatorcat.genever.core

import de.creatorcat.genever.core.ReleaseType.SNAPSHOT
import de.creatorcat.pard.cova.annotation.ImplementsArtifact

/**
 * Builder for a [VersionGenerationSpec].
 * The initial builder has [default style][useDefaultStyle].
 * See the property docs for other default values.
 *
 * Not every property of [VersionGenerationSpec] can be set freely
 * to avoid values that are not supported by common tool chains.
 * Instead, methods are provided to choose one of the *styles* used by certain frameworks (Eclipse, MS-Net...).
 *
 * The convenience [generate] method or the usual [with]/[apply] should be used for configuration,
 * before creating a spec instance via [build].
 *
 * Sample:
 *
 *     version.generate {
 *         releaseType = FINAL
 *         useEclipseStyle()
 *     }
 */
@ImplementsArtifact("reqVersionGeneration")
open class VersionGenerationSpecBuilder {
    /** value of [VersionGenerationSpec.releaseType] in the spec to be [build]; defaults to [SNAPSHOT] */
    open var releaseType: ReleaseType = SNAPSHOT
    /** value of [VersionGenerationSpec.preferAbbreviations] in the spec to be [build]; defaults to `true` */
    @ImplementsArtifact("reqFormatDefault", "reqFormatEclipse")
    open var preferAbbreviations: Boolean = true
    /** value of [VersionGenerationSpec.ensure3ComponentBase] in the spec to be [build]; defaults to `false` */
    @ImplementsArtifact("reqFormatEclipse")
    open var ensure3ComponentBase: Boolean = false
    /** value of [VersionGenerationSpec.upCaseCharacters] in the spec to be [build]; defaults to `false` */
    open var upCaseCharacters: Boolean = false
    /** value of [VersionGenerationSpec.forceNonEmptyQualifier] in the spec to be [build]; defaults to `false` */
    @ImplementsArtifact("reqFormatEclipse")
    open var forceNonEmptyQualifier: Boolean = false

    private var qualifierSeparator: String = "-"
    private var qualifierAsNumber: Boolean = false
    private var lexicographicalQualifier: Boolean = false

    /**
     * Set appropriate properties of the spec to be [build] to support a numeric qualifier:
     * * [VersionGenerationSpec.qualifierAsNumber] `= true`
     * * [VersionGenerationSpec.lexicographicalQualifier] `= false`
     *
     * Numeric qualifiers are necessary when generating MS-Net assembly versions.
     *
     * @see useMSNetStyle
     */
    fun useNumericQualifier() {
        qualifierAsNumber = true
        lexicographicalQualifier = false
    }

    /**
     * Set appropriate properties of the spec to be [build] to support a lexicographically ordered qualifier:
     * * [VersionGenerationSpec.lexicographicalQualifier] `= true`
     * * [VersionGenerationSpec.qualifierAsNumber] `= false`
     *
     * Lexicographical order is necessary when generating Eclipse plugin versions.
     *
     * @see useEclipseStyle
     */
    @ImplementsArtifact("reqFormatEclipse")
    fun useLexicographicalQualifier() {
        qualifierAsNumber = false
        lexicographicalQualifier = true
    }

    /**
     * Set default qualifier properties of the spec to be [build]:
     * * [VersionGenerationSpec.lexicographicalQualifier] `= false`
     * * [VersionGenerationSpec.qualifierAsNumber] `= false`
     *
     * The qualifier string will be generated exactly as defined in the original qualifier.
     *
     * @see useDefaultStyle
     */
    @ImplementsArtifact("reqFormatDefault")
    fun useDefaultQualifier() {
        qualifierAsNumber = false
        lexicographicalQualifier = false
    }

    /**
     * Set the [VersionGenerationSpec.qualifierSeparator] of the spec to be [build] to a hyphen ('-').
     * This is the default style in the Maven & Gradle world and also complies to SemVer.
     *
     * @see useDefaultStyle
     */
    @ImplementsArtifact("reqFormatDefault")
    fun useHyphenSeparator() {
        qualifierSeparator = "-"
    }

    /**
     * Set the [VersionGenerationSpec.qualifierSeparator] of the spec to be [build] to a dot ('.').
     * This is the mandatory style in the Eclipse and MS-Net world.
     *
     * @see useEclipseStyle
     * @see useMSNetStyle
     */
    @ImplementsArtifact("reqFormatEclipse")
    fun useDotSeparator() {
        qualifierSeparator = "."
    }

    /**
     * Set appropriate properties of the spec to be [build] to support Eclipse plugin version generation:
     * * [useDotSeparator]
     * * [useLexicographicalQualifier]
     * * [ensure3ComponentBase] `= true`
     * * [forceNonEmptyQualifier] `= true`
     */
    @ImplementsArtifact("reqFormatEclipse")
    fun useEclipseStyle() {
        useDotSeparator()
        useLexicographicalQualifier()
        ensure3ComponentBase = true
        forceNonEmptyQualifier = true
    }

    /**
     * Set appropriate properties of the spec to be [build] to support MS-Net assembly version generation:
     * * [useDotSeparator]
     * * [useNumericQualifier]
     * * [ensure3ComponentBase] `= true`
     */
    fun useMSNetStyle() {
        useDotSeparator()
        useNumericQualifier()
        ensure3ComponentBase = true
    }

    /**
     * Set default properties of the spec to be [build] (complying to Maven, Gradle & SemVer):
     * * [useHyphenSeparator]
     * * [useDefaultQualifier]
     * * [ensure3ComponentBase] `= false`
     * * [forceNonEmptyQualifier] `= false`
     */
    @ImplementsArtifact("reqFormatDefault")
    fun useDefaultStyle() {
        useHyphenSeparator()
        useDefaultQualifier()
        ensure3ComponentBase = false
        forceNonEmptyQualifier = false
    }

    /**
     * Build a [VersionGenerationSpec] using the current properties of this builder.
     * Each new spec will be a unique immutable object.
     * However, the creates specs are data classes
     * and thus provide proper `toString`, `equals` and `hashCode` methods.
     */
    open fun build(): VersionGenerationSpec = SimpleVersionGenerationSpec(
        releaseType,
        qualifierSeparator,
        forceNonEmptyQualifier,
        qualifierAsNumber,
        lexicographicalQualifier,
        preferAbbreviations,
        ensure3ComponentBase,
        upCaseCharacters
    )

    private data class SimpleVersionGenerationSpec(
        override val releaseType: ReleaseType,
        override val qualifierSeparator: String,
        override val forceNonEmptyQualifier: Boolean,
        override val qualifierAsNumber: Boolean,
        override val lexicographicalQualifier: Boolean,
        override val preferAbbreviations: Boolean,
        override val ensure3ComponentBase: Boolean,
        override val upCaseCharacters: Boolean
    ) : VersionGenerationSpec
}

/**
 * Convenience method to generate the version string
 * with the specification configured in the given [configureSpecBlock].
 *
 * Creates a [builder][VersionGenerationSpecBuilder], applies [configureSpecBlock]
 * and [builds][VersionGenerationSpecBuilder.build] the spec.
 * Then calls the [actual generate][GenericVersion.generate] with the created spec.
 */
@ExperimentalUnsignedTypes
fun GenericVersion<*>.generate(configureSpecBlock: VersionGenerationSpecBuilder.() -> Unit): String =
    generate(VersionGenerationSpec.of(configureSpecBlock))

/**
 * Convenience method to generate the qualifier string
 * with the specification configured in the given [configureSpecBlock].
 *
 * Creates a [builder][VersionGenerationSpecBuilder], applies [configureSpecBlock]
 * and [builds][VersionGenerationSpecBuilder.build] the spec.
 * Then calls the [actual generate][VersionQualifier.generate] with the created spec.
 */
fun VersionQualifier<*>.generate(configureSpecBlock: VersionGenerationSpecBuilder.() -> Unit): String =
    generate(VersionGenerationSpec.of(configureSpecBlock))

/**
 * Convenience method to create a [VersionGenerationSpec]
 * with the properties configured in the given [configureSpecBlock].
 *
 * Creates a [builder][VersionGenerationSpecBuilder], applies [configureSpecBlock]
 * and [builds][VersionGenerationSpecBuilder.build] the spec.
 */
fun VersionGenerationSpec.Companion.of(
    configureSpecBlock: VersionGenerationSpecBuilder.() -> Unit
): VersionGenerationSpec {
    val builder = VersionGenerationSpecBuilder()
    builder.configureSpecBlock()
    return builder.build()
} 
