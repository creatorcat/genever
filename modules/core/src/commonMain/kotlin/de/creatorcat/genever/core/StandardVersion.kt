/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file. 
 */

package de.creatorcat.genever.core

import de.creatorcat.kotiya.core.structures.makeMutationSafe
import de.creatorcat.pard.cova.annotation.ImplementsArtifact

/**
 * Standard immutable implementation of [GenericVersion]
 * that uses [compareBase] for calculating [version equality][equals]
 * before delegating to the respective method of the qualifier type [Q].
 *
 * It also provides proper [hashCode] and [toString] methods.
 */
@ExperimentalUnsignedTypes
@ImplementsArtifact("reqVersionDef")
open class StandardVersion<Q : VersionQualifier<Q>>(
    override val qualifier: Q,
    @ImplementsArtifact("reqBaseVersionDef")
    base: List<UInt>
) : GenericVersion<Q> {

    override val base: List<UInt> = base.makeMutationSafe()

    // TODO this must be disabled to make kapt work 
    // TODO remove deprecation tag once bug is fixed
//    @Deprecated("not usable due to bug: https://youtrack.jetbrains.com/issue/KT-28855")
//    constructor(qualifier: Q, @ImplementsArtifact("reqBaseVersionDef") vararg baseComponents: UInt) :
//    this(qualifier, baseComponents.toList())

    /**
     * The object [other] is considered equal, iff:
     * * it is a [StandardVersion]
     * * `this.compareBase(other) == 0`
     * * `this.qualifier == other.qualifier`
     *
     * @see compareBase
     * @see qualifier
     */
    override fun equals(other: Any?): Boolean {
        if (other !is StandardVersion<*>) return false
        return compareBase(other) == 0
            && qualifier == other.qualifier
    }

    override fun hashCode(): Int = 17 * base.reduceIndexed { componentIndex, acc, componentLevel ->
        // for componentLevel == 0 nothing will be added, so version "1" has same code as "1.0.0"
        acc + (componentIndex + 1).toUInt() * componentLevel
    }.toInt() + 23 * qualifier.hashCode()

    override fun toString(): String = "StandardVersion(base=$base, qualifier=$qualifier)"

    /** The companion object provides static creation methods. */
    companion object
}
