/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.core.reqtest

import de.creatorcat.kotiya.test.core.io.FileCreatingTestBase

/**
 * Uses the mechanism of [FileCreatingTestBase] to provide a proper place to put the comparison log file.
 */
private class VersionComparisonTestLog : FileCreatingTestBase {

    val logFile = testOutFile("comparison.log")

    init {
        setup()
        logFile.createNewFile()
    }
}

private val log = VersionComparisonTestLog()

internal actual fun logVersionComparison(line: String) {
    log.logFile.appendText("$line\n")
}
