/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.core.reqtest

import de.creatorcat.genever.core.VersionGenerationSpecBuilder
import de.creatorcat.pard.cova.annotation.VerifiesArtifact
import org.gradle.api.internal.artifacts.ivyservice.ivyresolve.Versioned
import org.gradle.api.internal.artifacts.ivyservice.ivyresolve.strategy.DefaultVersionComparator
import org.gradle.api.internal.artifacts.ivyservice.ivyresolve.strategy.Version
import org.gradle.api.internal.artifacts.ivyservice.ivyresolve.strategy.VersionParser

/**
 * Version comparison test based on the actual Gradle versioning scheme.
 */
@ExperimentalUnsignedTypes
@VerifiesArtifact("reqFormatDefault")
class GradleStdVersionComparisonTest : StandardVersionComparisonTestBase<GradleVersioned>() {
    override fun provideBasicGenerationSpec(): VersionGenerationSpecBuilder {
        return VersionGenerationSpecBuilder().apply {
            useDefaultStyle()
        }
    }

    override fun generateComparableVersion(versionString: String): GradleVersioned =
        GradleVersioned(versionString)
}

class GradleVersioned(private val versionString: String) : Versioned, Comparable<GradleVersioned> {
    override fun compareTo(other: GradleVersioned) = comparator.compare(this, other)

    override fun getVersion(): Version = parser.transform(versionString)

    companion object {
        private val comparator = DefaultVersionComparator()
        private val parser = VersionParser()
    }
}
