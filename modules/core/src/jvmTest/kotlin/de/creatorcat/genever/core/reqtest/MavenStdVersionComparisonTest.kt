/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.core.reqtest

import de.creatorcat.genever.core.VersionGenerationSpecBuilder
import de.creatorcat.pard.cova.annotation.VerifiesArtifact
import org.apache.maven.artifact.versioning.ComparableVersion

/**
 * Version comparison test based on the actual Maven versioning scheme.
 */
@ExperimentalUnsignedTypes
@VerifiesArtifact("reqFormatDefault")
class MavenStdVersionComparisonTest : StandardVersionComparisonTestBase<ComparableVersion>() {
    override fun provideBasicGenerationSpec(): VersionGenerationSpecBuilder {
        return VersionGenerationSpecBuilder().apply {
            useDefaultStyle()
        }
    }

    override fun generateComparableVersion(versionString: String): ComparableVersion =
        ComparableVersion(versionString)
}
