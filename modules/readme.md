# GeneVer Modules

## Structure

The project consists of the following modules, each in its own directory:
* [core](https://creatorcat.gitlab.io/genever/docs/core/html/genever-core)
* [gradle](https://creatorcat.gitlab.io/genever/docs/gradle/html/genever-gradle)

Each module directory contains:
* `doc/`: module specific documentation (optional)
* `src/`: sources and resources in the standard Gradle/Kotlin layout
* `build.gradle`: module build file - 
  applies the proper configuration (see [devdoc](../doc/devdoc.md#build-and-configuration))
* `module.md` contains general module and package documentation

Module naming convention:
* Module names are lower case alphanumeric words
* Multiple words may be separated by a hyphen ('-')
* If there is a module `libname` then there must be no separate module named `libname-subname`.
  This name pattern is reserved for special modules supporting `libname` e.g `libname-functests`.  
