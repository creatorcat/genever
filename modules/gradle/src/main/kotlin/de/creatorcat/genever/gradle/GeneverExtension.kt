/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.gradle

import de.creatorcat.kotiya.gradle.withClosureOf
import groovy.lang.Closure
import org.gradle.api.Project

/**
 * Main Gradle script extension of the [GeneverPlugin].
 * Provides convenience functionality to work with Genever versions.
 *
 * See the user guide for further information.
 */
open class GeneverExtension(
    /** Associated Gradle project */
    val project: Project
) {

    /**
     * Gradle script convenience method to create a standard version object with the given [configuration].
     *
     * Creates a [builder][GradleStandardVersionBuilder], applies the [configuration]
     * and [builds][GradleStandardVersionBuilder.build] the version.
     */
    @ExperimentalUnsignedTypes
    fun standardVersion(
        configuration: Closure<Unit>
    ): GradleStandardVersion =
        withClosureOf(GradleStandardVersionBuilder(project), configuration) {
            build()
        }

}
