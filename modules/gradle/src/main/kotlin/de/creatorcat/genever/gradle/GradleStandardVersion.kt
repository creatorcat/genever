/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.gradle

import de.creatorcat.genever.core.StandardQualifier
import de.creatorcat.genever.core.StandardVersion
import de.creatorcat.kotiya.gradle.withClosureOf
import de.creatorcat.kotiya.matchers.common.isPositive
import de.creatorcat.kotiya.matchers.require
import groovy.lang.Closure
import org.gradle.api.Project

/**
 * Extension of [StandardVersion] that adds necessary functionality to
 * make usage in Gradle build scripts more convenient and comfortable,
 * especially considering Groovy.
 */
@ExperimentalUnsignedTypes
class GradleStandardVersion(
    /** Associated Gradle project */
    val project: Project,
    qualifier: StandardQualifier,
    base: List<UInt>
) : StandardVersion<StandardQualifier>(qualifier, base) {

    /**
     * Gradle script convenience method to generate the version string
     * with the specification configured in the given [specConfiguration].
     *
     * Creates a [builder][GradleVersionGenerationSpecBuilder], applies [specConfiguration]
     * and [builds][GradleVersionGenerationSpecBuilder.build] the spec.
     * Then calls the [actual generate][GradleStandardVersion.generate] with the created spec.
     */
    fun generate(specConfiguration: Closure<Unit>): String =
        generate(withClosureOf(GradleVersionGenerationSpecBuilder(project), specConfiguration) {
            build()
        })

    /**
     * Gradle script convenience method to retrieve [levelOfBaseComponent][StandardVersion.levelOfBaseComponent]
     * with a signed integer index.
     */
    fun levelOfBaseComponent(index: Int): UInt =
        levelOfBaseComponent(require("base component index", index, isPositive))

    /**
     * Gradle script convenience method to create a copy of this version
     * but with applying the [adjustment] closure on a [GradleStandardVersionBuilder].
     * The builder will be initialized with the values of this version.
     *
     * Example:
     *
     *     val otherVersion = myVersion.copyWith {
     *         majorLevel = 2u
     *         patchLevel = 1u
     *     }
     *
     * @param project the associated project of the created version;
     *   defaults to [this.project][GradleStandardVersion.project]
     */
    @JvmOverloads
    fun copyWith(project: Project = this.project, adjustment: Closure<Unit>): GradleStandardVersion {
        val templateVersion = this
        val builder = GradleStandardVersionBuilder(project).apply {
            qualifier = templateVersion.qualifier
            setBase(templateVersion.base.map { it.toInt() })
        }
        return withClosureOf(builder, adjustment) {
            build()
        }
    }
}
