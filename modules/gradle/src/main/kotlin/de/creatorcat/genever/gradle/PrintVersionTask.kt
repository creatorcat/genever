/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.plugins.HelpTasksPlugin
import org.gradle.api.tasks.TaskAction

/**
 * A task to print the version property of the project.
 * Useful to verify the project's GeneVer configuration.
 */
open class PrintVersionTask : DefaultTask() {

    init {
        description = "Outputs the version property to verify genever configuration"
        group = HelpTasksPlugin.HELP_GROUP
    }

    /**
     * Prints the version via [logger.lifecycle][logger].
     */
    @TaskAction
    fun printVersion() {
        logger.lifecycle("{}: {}", project.name, project.version)
    }
}
