/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.gradle

import de.creatorcat.genever.core.GenericVersionBuilder
import de.creatorcat.genever.core.MutableBaseComponents
import de.creatorcat.genever.core.StandardQualifier
import de.creatorcat.kotiya.core.structures.setAll
import de.creatorcat.kotiya.gradle.withClosureOf
import de.creatorcat.kotiya.matchers.common.isPositive
import de.creatorcat.kotiya.matchers.require
import groovy.lang.Closure
import org.gradle.api.Project
import java.util.Collections

/**
 * Extension of [GenericVersionBuilder] for [GradleStandardVersion]s that adds necessary functionality to
 * make usage in Gradle build scripts more convenient and comfortable,
 * especially considering Groovy.
 */
@ExperimentalUnsignedTypes
class GradleStandardVersionBuilder(
    /** Associated Gradle project */
    val project: Project
) : GenericVersionBuilder<StandardQualifier, GradleStandardVersion>() {

    override fun build(): GradleStandardVersion =
        GradleStandardVersion(
            project,
            checkNotNull(qualifier) { "no qualifier defined" },
            base.components
        )

    override val base: GradleMutableBaseComponents = GradleMutableBaseComponents()

    /**
     * Gradle script convenience method to replace all [base] components with the given [newComponents].
     */
    fun setBase(newComponents: List<Int>) {
        base.setAll(*newComponents.toIntArray())
    }

    /**
     * Gradle script convenience method to set the [majorLevel] with a signed integer.
     */
    fun setMajorLevel(value: Int) {
        majorLevel = require("major level", value, isPositive)
    }

    /**
     * Gradle script convenience method to set the [minorLevel] with a signed integer.
     */
    fun setMinorLevel(value: Int) {
        minorLevel = require("minor level", value, isPositive)
    }

    /**
     * Gradle script convenience method to set the [patchLevel] with a signed integer.
     */
    fun setPatchLevel(value: Int) {
        patchLevel = require("patch level", value, isPositive)
    }

    /**
     * Gradle script convenience method to set this versions [qualifier] to a new [StandardQualifier]
     * created by a [GradleStandardQualifierBuilder] with applied [configuration].
     */
    fun qualifierOf(
        configuration: Closure<Unit>
    ) {
        qualifier = withClosureOf(GradleStandardQualifierBuilder(), configuration) {
            build()
        }
    }

    /**
     * The [qualifier of the version][GradleStandardVersion.qualifier] to be [build].
     * Defaults to the trivial [StandardQualifier] with all defaults.
     */
    override var qualifier: StandardQualifier? = StandardQualifier()
}

/**
 * Extension of [MutableBaseComponents] that adds necessary functionality to
 * make usage in Gradle build scripts more convenient and comfortable,
 * especially considering Groovy.
 */
@ExperimentalUnsignedTypes
class GradleMutableBaseComponents : MutableBaseComponents() {

    /**
     * The list of currently defined components.
     * This list is read only! Modifications will lead to exceptions!
     */
    override val components: MutableList<UInt> = Collections.unmodifiableList(internalComponents)

    /**
     * Gradle script convenience method to make the indexed access operator `[ ]` usable in Groovy
     * to define base components.
     * If [index] is above the current size, missing components will be filled up with zeros.
     */
    fun putAt(index: Int, value: Int) {
        while (internalComponents.size <= index) internalComponents.add(0u)
        internalComponents[index] = require("level of base component $index", value, isPositive)
    }

    /**
     * Gradle script convenience method to make the indexed access operator `[ ]` usable in Groovy
     * to read base components.
     * Unlike the [putAt]-operator, retrieving an undefined component will lead to an [IndexOutOfBoundsException].
     */
    fun getAt(index: Int): UInt = internalComponents[index]

    /**
     * Gradle script convenience method to replace all current components by the [newComponents],
     * supporting signed integers which must be positive, of course.
     */
    fun setAll(vararg newComponents: Int): Unit =
        internalComponents.setAll(newComponents.map { require("base component", it, isPositive) })
}
