/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.gradle

import de.creatorcat.genever.core.ReleaseType
import de.creatorcat.genever.core.VersionGenerationSpecBuilder
import org.gradle.api.Project

/**
 * Extension of [VersionGenerationSpecBuilder] that adds necessary functionality to
 * make usage in Gradle build scripts more convenient and comfortable,
 * especially considering Groovy.
 */
class GradleVersionGenerationSpecBuilder(
    /** Associated Gradle project */
    val project: Project
) : VersionGenerationSpecBuilder() {

    init {
        val releaseTypeString = project.findProperty("genever.releaseType")
        if (releaseTypeString is String) {
            releaseType = ReleaseType.valueOf(releaseTypeString.toUpperCase())
        }
    }

    companion object {

        /** [ReleaseType.SNAPSHOT] constant to avoid imports in Gradle scripts.*/
        @JvmStatic
        val SNAPSHOT: ReleaseType = ReleaseType.SNAPSHOT

        /** [ReleaseType.INTERMEDIATE] constant to avoid imports in Gradle scripts.*/
        @JvmStatic
        val INTERMEDIATE: ReleaseType = ReleaseType.INTERMEDIATE

        /** [ReleaseType.FINAL] constant to avoid imports in Gradle scripts.*/
        @JvmStatic
        val FINAL: ReleaseType = ReleaseType.FINAL
    }
}
