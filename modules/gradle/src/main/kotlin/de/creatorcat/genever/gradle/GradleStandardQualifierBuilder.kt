/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.gradle

import de.creatorcat.genever.core.StandardQualifier.Phase.ALPHA
import de.creatorcat.genever.core.StandardQualifier.Phase.BETA
import de.creatorcat.genever.core.StandardQualifier.Phase.MILESTONE
import de.creatorcat.genever.core.StandardQualifier.Phase.RELEASE_CANDIDATE
import de.creatorcat.genever.core.StandardQualifierBuilder
import de.creatorcat.kotiya.matchers.common.isPositive
import de.creatorcat.kotiya.matchers.require

/**
 * Extension of [StandardQualifierBuilder] that adds necessary functionality to
 * make usage in Gradle build scripts more convenient and comfortable,
 * especially considering Groovy.
 */
@ExperimentalUnsignedTypes
class GradleStandardQualifierBuilder : StandardQualifierBuilder() {

    /**
     * Gradle script convenience method to set the [phaseLevel] with a signed integer.
     */
    fun setPhaseLevel(level: Int) {
        phaseLevel = require("phase level", level, isPositive)
    }

    /**
     * Gradle script convenience method to define [phase] to be [ALPHA]
     * and [phaseLevel] to signed integer [level] (defaults to `1`).
     */
    @JvmOverloads
    fun phaseAlpha(level: Int = 1): Unit = phaseAlpha(require("phase level", level, isPositive))

    /**
     * Gradle script convenience method to define [phase] to be [BETA]
     * and [phaseLevel] to signed integer [level] (defaults to `1`).
     */
    @JvmOverloads
    fun phaseBeta(level: Int = 1): Unit = phaseBeta(require("phase level", level, isPositive))

    /**
     * Gradle script convenience method to define [phase] to be [MILESTONE]
     * and [phaseLevel] to signed integer [level] (defaults to `1`).
     */
    @JvmOverloads
    fun phaseMilestone(level: Int = 1): Unit = phaseMilestone(require("phase level", level, isPositive))

    /**
     * Gradle script convenience method to define [phase] to be [RELEASE_CANDIDATE]
     * and [phaseLevel] to signed integer [level] (defaults to `1`).
     */
    @JvmOverloads
    fun phaseRC(level: Int = 1): Unit = phaseRC(require("phase level", level, isPositive))

}
