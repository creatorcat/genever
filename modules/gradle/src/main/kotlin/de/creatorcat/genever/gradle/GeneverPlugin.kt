/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file. 
 */

package de.creatorcat.genever.gradle

import de.creatorcat.kotiya.core.text.CREATOR_CAT_CLI_HELLO
import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * GeneVer Gradle plugin adding the [genever extension][GeneverExtension]
 * and the [printVersion task][PrintVersionTask].
 *
 * See the user guide for further information.
 */
class GeneverPlugin : Plugin<Project> {

    /**
     * Apply the plugin to the [target] project.
     *
     * @see [Plugin.apply]
     */
    override fun apply(target: Project) {
        target.logger.info(CREATOR_CAT_CLI_HELLO)
        target.extensions.create(MAIN_EXTENSION_NAME, GeneverExtension::class.java, target)
        target.tasks.register(PRINT_VERSION_TASK_NAME, PrintVersionTask::class.java)
    }

    companion object {
        /** The plugin ID - used when applying the plugin in a build script */
        const val ID: String = "de.creatorcat.genever"
        /** The name of the genever main extension */
        const val MAIN_EXTENSION_NAME: String = "genever"
        /** The name of the default [PrintVersionTask] */
        const val PRINT_VERSION_TASK_NAME: String = "printVersion"
    }
}
