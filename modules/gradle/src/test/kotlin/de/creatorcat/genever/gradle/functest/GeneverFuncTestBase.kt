/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.gradle.functest

import de.creatorcat.genever.gradle.GeneverPlugin
import de.creatorcat.kotiya.test.gradle.FunctionalGradleTestBase

abstract class GeneverFuncTestBase : FunctionalGradleTestBase(GeneverPlugin.ID) {

    protected fun printAssertableVersion(versionObjectName: String) =
        printAssertableValue("${versionObjectName}.generate {}", versionObjectName)

    protected fun runAndAssertPrintedValues(
        vararg tagValuePairs: Pair<String, Any?>, additionalArgs: Array<String> = emptyArray()
    ) {
        val result = runAndAssertTask("projects", "--stacktrace", *additionalArgs)
        result.assertOutputValues(*tagValuePairs)
    }
}
