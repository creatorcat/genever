/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.gradle

import de.creatorcat.kotiya.matchers.common.isInstanceOf
import de.creatorcat.kotiya.test.core.assertThat
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import kotlin.test.Test
import kotlin.test.assertTrue

class GeneverPluginTest {

    private val project: Project = with(ProjectBuilder.builder()) {
        val project = build()
        project.pluginManager.apply(GeneverPlugin.ID)
        project
    }

    @Test fun `plugin - can be applied`() {
        assertTrue(project.pluginManager.hasPlugin(GeneverPlugin.ID))
    }

    @Test fun `main extension - is defined with proper id`() {
        assertThat(
            project.extensions.getByName(GeneverPlugin.MAIN_EXTENSION_NAME),
            isInstanceOf<GeneverExtension>()
        )
    }

    @Test fun `print version task - is defined with proper id`() {
        assertThat(
            project.tasks.getByPath(":${GeneverPlugin.PRINT_VERSION_TASK_NAME}"),
            isInstanceOf<PrintVersionTask>()
        )
    }
}
