/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.gradle.functest

import kotlin.test.Test

class DefineStandardVersions : GeneverFuncTestBase() {

    @Test fun `minimum example - all defaults`() {
        writeBuildScript(
            """
            def testVersion = genever.standardVersion {}

            ${printAssertableVersion("testVersion")}
            """
        )
        runAndAssertPrintedValues("testVersion" to "0-rc1-SNAPSHOT")
    }

    @Test fun `access version properties`() {
        writeBuildScript(
            """
            def testVersion = genever.standardVersion {
                base = [1,2,3,4,5]
            }
            
            def major = testVersion.majorLevel
            def level4 = testVersion.base[4]
            def level6 = testVersion.levelOfBaseComponent(6)
            def qPhaseLevel = testVersion.qualifier.phaseLevel
            
            ${printAssertableVersion("testVersion")}
            ${printAssertableValue("major")}
            ${printAssertableValue("level4")}
            ${printAssertableValue("level6")}
            ${printAssertableValue("qPhaseLevel")}
            """
        )
        runAndAssertPrintedValues(
            "testVersion" to "1.2.3.4.5-rc1-SNAPSHOT",
            "major" to 1, "level4" to 5, "level6" to 0, "qPhaseLevel" to 1
        )
    }

    @Test fun `configure version`() {
        writeBuildScript(
            """
            def v1 = genever.standardVersion {
                base = [1,2,3]
            }
            ${printAssertableVersion("v1")}

            def v2 = genever.standardVersion {
                patchLevel = 2
                minorLevel = 3
                majorLevel = 4
                base[4] = 5
            }
            ${printAssertableVersion("v2")}

            def v3 = genever.standardVersion {
                base.setAll(1,2,3)
            }
            ${printAssertableVersion("v3")}
            """
        )
        runAndAssertPrintedValues(
            "v1" to "1.2.3-rc1-SNAPSHOT",
            "v2" to "4.3.2.0.5-rc1-SNAPSHOT",
            "v3" to "1.2.3-rc1-SNAPSHOT"
        )
    }

    @Test fun `configure qualifier`() {
        writeBuildScript(
            """
            def v1 = genever.standardVersion {
                qualifierOf {
                    phaseLevel = 3
                    releaseMetadata = "MD"
                }
            }
            ${printAssertableValue("""v1.generate { releaseType = INTERMEDIATE }""", "v1")}

            def v2 = genever.standardVersion {
                qualifierOf {
                    phaseBeta(2)
                    intermediateReleaseMetadata = "MD"
                }
            }
            ${printAssertableValue("""v2.generate { releaseType = INTERMEDIATE }""", "v2")}
            """
        )
        runAndAssertPrintedValues(
            "v1" to "0-rc3-vMD",
            "v2" to "0-b2-vMD"
        )
    }

    @Test fun `copy version`() {
        writeBuildScript(
            """
            def v1 = genever.standardVersion {
                base = [1,2,3]
            }
            ${printAssertableVersion("v1")}

            def v2 = v1.copyWith {
                majorLevel = 3
                patchLevel = 0
            }
            ${printAssertableVersion("v2")}

            def v3 = v1.copyWith(project) {
                majorLevel = 2
                patchLevel = 2
            }
            ${printAssertableVersion("v3")}
            """
        )
        runAndAssertPrintedValues(
            "v1" to "1.2.3-rc1-SNAPSHOT",
            "v2" to "3.2.0-rc1-SNAPSHOT",
            "v3" to "2.2.2-rc1-SNAPSHOT"
        )
    }
}
