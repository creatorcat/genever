/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.gradle

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import de.creatorcat.genever.core.ReleaseType
import org.gradle.api.Project
import kotlin.test.Test
import kotlin.test.assertEquals

class GradleVersionGenerationSpecBuilderTest {

    @Test fun `releaseType - constant values`() {
        assertEquals(ReleaseType.SNAPSHOT, GradleVersionGenerationSpecBuilder.SNAPSHOT)
        assertEquals(ReleaseType.INTERMEDIATE, GradleVersionGenerationSpecBuilder.INTERMEDIATE)
        assertEquals(ReleaseType.FINAL, GradleVersionGenerationSpecBuilder.FINAL)
    }

    @Test fun `releaseType - initialization via project property`() {

        assertEquals(
            ReleaseType.SNAPSHOT,
            GradleVersionGenerationSpecBuilder(mock<Project>()).releaseType
        )

        val project = mock<Project> {
            on { findProperty(eq("genever.releaseType")) } doReturn "intermediate"
        }

        assertEquals(
            ReleaseType.INTERMEDIATE,
            GradleVersionGenerationSpecBuilder(project).releaseType
        )
    }
}
