/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.gradle

import com.nhaarman.mockitokotlin2.mock
import de.creatorcat.genever.core.StandardQualifier
import de.creatorcat.genever.core.generate
import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.gradle.DummyClosure
import org.gradle.api.Project
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame

@ExperimentalUnsignedTypes
class GradleStandardVersionTest {

    @Test fun `creation - initializes properties`() {
        val qualifier = StandardQualifier()
        val version = GradleStandardVersion(mock<Project>(), qualifier, listOf(1u, 2u, 3u))
        assertSame(qualifier, version.qualifier)
        assertThat(version.base, hasElementsEqual(1u, 2u, 3u))
    }

    @Test fun `levelOfBaseComponent - with signed int`() {
        with(GradleStandardVersion(mock<Project>(), StandardQualifier(), listOf(1u, 2u, 3u))) {
            assertEquals(1u, levelOfBaseComponent(0))
            assertEquals(2u, levelOfBaseComponent(1))
            assertEquals(3u, levelOfBaseComponent(2))
            assertEquals(0u, levelOfBaseComponent(3))
        }
    }

    @Test fun `generate - with trivial closure`() {
        // non-trivial closure tests are found in functional tests
        val version = GradleStandardVersion(mock<Project>(), StandardQualifier(), listOf(1u, 2u))
        val generatedWithSpec = version.generate {}
        val generatedWithClosure = version.generate(DummyClosure.of<Unit, GradleVersionGenerationSpecBuilder> {})
        assertEquals(generatedWithSpec, generatedWithClosure)
    }

    @Test fun `copyWith - standard usage`() {
        val q1 = StandardQualifier()
        val q2 = StandardQualifier()
        val project = mock<Project>()
        val original = GradleStandardVersion(project, q1, listOf(1u, 2u, 3u))
        val copy1 = original.copyWith(adjustment = DummyClosure.of<Unit, GradleStandardVersionBuilder> {
            majorLevel = 2u
            patchLevel = 0u
            qualifier = q2
        })
        assertThat(original.base, hasElementsEqual(1u, 2u, 3u))
        assertSame(q1, original.qualifier)
        assertSame(project, original.project)

        assertThat(copy1.base, hasElementsEqual(2u, 2u, 0u))
        assertSame(q2, copy1.qualifier)
        assertSame(project, copy1.project)

        val newProject = mock<Project>()
        val copy2 = original.copyWith(newProject, DummyClosure.of<Unit, GradleStandardVersionBuilder> {
            majorLevel = 2u
        })
        assertThat(original.base, hasElementsEqual(1u, 2u, 3u))
        assertSame(q1, original.qualifier)
        assertSame(project, original.project)

        assertThat(copy2.base, hasElementsEqual(2u, 2u, 3u))
        assertSame(q1, copy2.qualifier)
        assertSame(newProject, copy2.project)
    }

}
