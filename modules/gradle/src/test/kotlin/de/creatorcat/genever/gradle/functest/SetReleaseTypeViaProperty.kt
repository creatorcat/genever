/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.gradle.functest

import kotlin.test.Test

class SetReleaseTypeViaProperty : GeneverFuncTestBase() {

    @Test fun `property via command line`() {
        writeBuildScript(
            """
            ${printAssertableValue("""findProperty("genever.releaseType")""", "genever.releaseType")}
                
            def testVersion = genever.standardVersion {
                base = [1,2,3]
            }
            ${printAssertableVersion("testVersion")}
            """
        )
        runAndAssertPrintedValues(
            "genever.releaseType" to "intermediate",
            "testVersion" to "1.2.3-rc1",
            additionalArgs = arrayOf("-Pgenever.releaseType=intermediate")
        )
    }
}
