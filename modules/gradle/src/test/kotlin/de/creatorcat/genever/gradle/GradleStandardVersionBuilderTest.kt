/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */


package de.creatorcat.genever.gradle

import com.nhaarman.mockitokotlin2.mock
import de.creatorcat.genever.core.StandardQualifier
import de.creatorcat.genever.core.StandardQualifier.Phase
import de.creatorcat.genever.core.StandardQualifierBuilder
import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.core.assertThrows
import de.creatorcat.kotiya.test.gradle.DummyClosure
import org.gradle.api.Project
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertSame

@ExperimentalUnsignedTypes
class GradleStandardVersionBuilderTest {

    private val builder = GradleStandardVersionBuilder(mock<Project>())
    private val base = GradleMutableBaseComponents()

    @Test fun `BaseComponents - components list is unmodifiable`() {
        base.setAll(1u, 2u, 3u)
        assertThrows<UnsupportedOperationException> { base.components[1] = 4u }
        assertThrows<UnsupportedOperationException> { base.components.clear() }
        assertThrows<UnsupportedOperationException> { base.components.add(1u) }
    }

    @Test fun `BaseComponents - setAll`() {
        base.setAll(1, 2, 3)
        assertThat(base.components, hasElementsEqual(1u, 2u, 3u))
        base.setAll(17)
        assertThat(base.components, hasElementsEqual(17u))
        assertThrows<IllegalArgumentException> { base.setAll(1, 2, -1) }
    }

    @Test fun `BaseComponents - getAt`() {
        base.setAll(1u, 2u, 3u)
        assertEquals(1u, base.getAt(0))
        assertEquals(2u, base.getAt(1))
        assertEquals(3u, base.getAt(2))
        assertThrows<IndexOutOfBoundsException> { base.getAt(3) }
    }

    @Test fun `BaseComponents - putAt`() {
        base.putAt(2, 3)
        assertThat(base.components, hasElementsEqual(0u, 0u, 3u))
        base.putAt(0, 1)
        assertThat(base.components, hasElementsEqual(1u, 0u, 3u))
        base.putAt(4, 5)
        assertThat(base.components, hasElementsEqual(1u, 0u, 3u, 0u, 5u))
        assertThrows<IllegalArgumentException> { base.putAt(0, -1) }
    }

    @Test fun `base - set as signed list`() {
        builder.setBase(listOf(1, 2, 3, 4))
        assertThat(builder.base.components, hasElementsEqual(1u, 2u, 3u, 4u))
        builder.setBase(listOf(17))
        assertThat(builder.base.components, hasElementsEqual(17u))
        assertThrows<IllegalArgumentException> { builder.setBase(listOf(1, 2, -1)) }
    }

    @Test fun `build - uses properties`() {
        builder.setBase(listOf(1, 2, 3))
        builder.qualifier = StandardQualifier()
        val version = builder.build()
        assertSame(builder.qualifier, version.qualifier)
        assertThat(version.base, hasElementsEqual(1u, 2u, 3u))
    }

    @Test fun `majorLevel - set as signed`() {
        builder.setMajorLevel(1)
        assertThat(builder.base.components, hasElementsEqual(1u))
    }

    @Test fun `minorLevel - set as signed`() {
        builder.setMinorLevel(1)
        assertThat(builder.base.components, hasElementsEqual(0u, 1u))
    }

    @Test fun `patchLevel - set as signed`() {
        builder.setPatchLevel(1)
        assertThat(builder.base.components, hasElementsEqual(0u, 0u, 1u))
    }

    @Test fun `qualifierOf - with non-trivial closure`() {
        builder.qualifierOf(DummyClosure.of<Unit, StandardQualifierBuilder> {
            phaseAlpha(3u)
            releaseMetadata = "X"
        })
        assertNotNull(builder.qualifier) {
            assertEquals(Phase.ALPHA, it.phase)
            assertEquals(3u, it.phaseLevel)
            assertEquals("X", it.finalReleaseMetadata)
            assertEquals("X", it.intermediateReleaseMetadata)
        }
    }

    @Test fun `qualifier - default is trivial`() {
        assertNotNull(builder.qualifier) {
            assertEquals(Phase.RELEASE_CANDIDATE, it.phase)
            assertEquals(1u, it.phaseLevel)
            assertThat(it.finalReleaseMetadata, isEmpty)
            assertThat(it.intermediateReleaseMetadata, isEmpty)
        }
    }

}
