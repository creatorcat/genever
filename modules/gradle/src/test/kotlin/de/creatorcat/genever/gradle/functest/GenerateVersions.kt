/*
 * Copyright 2018 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.gradle.functest

import kotlin.test.Test

class GenerateVersions : GeneverFuncTestBase() {

    @Test fun `configure generation`() {
        writeBuildScript(
            """
            def version = genever.standardVersion {
                base = [1,2]
                qualifierOf {
                    phaseAlpha(3)
                    intermediateReleaseMetadata = "23"
                }
            }

            def v1 = version.generate {}
            ${printAssertableValue("v1")}

            def v2 = version.generate {
                releaseType = FINAL
                ensure3ComponentBase = true
                upCaseCharacters = true
                forceNonEmptyQualifier = true
                useDotSeparator()
            }
            ${printAssertableValue("v2")}

            def v3 = version.generate {
                releaseType = INTERMEDIATE
                preferAbbreviations = false
                useLexicographicalQualifier()
            }
            ${printAssertableValue("v3")}

            def v4 = version.generate {
                releaseType = INTERMEDIATE
                useMSNetStyle()
            }
            ${printAssertableValue("v4")}
            """
        )
        runAndAssertPrintedValues(
            "v1" to "1.2-a3-SNAPSHOT",
            "v2" to "1.2.0.RELEASE",
            "v3" to "1.2-alpha003-v23",
            "v4" to "1.2.0.03023"
        )
    }
}
