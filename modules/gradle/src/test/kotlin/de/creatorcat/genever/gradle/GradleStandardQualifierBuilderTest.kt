/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.gradle

import de.creatorcat.genever.core.StandardQualifier.Phase.ALPHA
import de.creatorcat.genever.core.StandardQualifier.Phase.BETA
import de.creatorcat.genever.core.StandardQualifier.Phase.MILESTONE
import de.creatorcat.genever.core.StandardQualifier.Phase.RELEASE_CANDIDATE
import de.creatorcat.kotiya.test.core.assertThrows
import kotlin.test.Test
import kotlin.test.assertEquals

@ExperimentalUnsignedTypes
class GradleStandardQualifierBuilderTest {

    private val builder = GradleStandardQualifierBuilder()

    @Test fun `phaseLevel - signed int setter`() {
        builder.setPhaseLevel(3)
        assertEquals(3u, builder.phaseLevel)
        assertThrows<IllegalArgumentException> { builder.setPhaseLevel(-1) }
    }

    @Test fun `phase shortcuts - alpha`() {
        builder.phaseAlpha(3)
        assertEquals(ALPHA, builder.phase)
        assertEquals(3u, builder.phaseLevel)
    }

    @Test fun `phase shortcuts - beta`() {
        builder.phaseBeta(3)
        assertEquals(BETA, builder.phase)
        assertEquals(3u, builder.phaseLevel)
    }

    @Test fun `phase shortcuts - milestone`() {
        builder.phaseMilestone(3)
        assertEquals(MILESTONE, builder.phase)
        assertEquals(3u, builder.phaseLevel)
    }

    @Test fun `phase shortcuts - release candidate`() {
        builder.phaseRC(3)
        assertEquals(RELEASE_CANDIDATE, builder.phase)
        assertEquals(3u, builder.phaseLevel)
    }
}
