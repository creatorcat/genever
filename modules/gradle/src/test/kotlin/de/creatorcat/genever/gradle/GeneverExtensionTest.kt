/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.gradle

import com.nhaarman.mockitokotlin2.mock
import de.creatorcat.genever.core.StandardQualifier
import de.creatorcat.kotiya.matchers.common.hasElementsEqual
import de.creatorcat.kotiya.test.core.assertThat
import de.creatorcat.kotiya.test.gradle.DummyClosure
import org.gradle.api.Project
import kotlin.test.Test
import kotlin.test.assertSame

@ExperimentalUnsignedTypes
class GeneverExtensionTest {

    @Test fun `standardVersion - with trivial closure`() {
        val testee = GeneverExtension(mock<Project>())
        val q = StandardQualifier()
        val version = testee.standardVersion(DummyClosure(GradleStandardVersionBuilder::class) {
            qualifier = q
            base.setAll(1, 2, 3)
        })
        assertSame(q, version.qualifier)
        assertThat(version.base, hasElementsEqual(1u, 2u, 3u))
    }
}
