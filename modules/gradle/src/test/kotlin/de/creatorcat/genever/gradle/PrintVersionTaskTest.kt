/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.gradle

import de.creatorcat.kotiya.matchers.common.isEmpty
import de.creatorcat.kotiya.matchers.common.isNotNull
import de.creatorcat.kotiya.matchers.not
import de.creatorcat.kotiya.matchers.operators.which
import de.creatorcat.kotiya.test.core.assertThat
import org.gradle.api.Project
import org.gradle.api.plugins.HelpTasksPlugin
import org.gradle.testfixtures.ProjectBuilder
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class PrintVersionTaskTest {

    private val project: Project = ProjectBuilder.builder().build()

    @Test fun `has proper description and group`() {
        with(project.tasks.create("testPrintVersion", PrintVersionTask::class.java)) {
            assertThat(description, isNotNull which !isEmpty)
            assertEquals(HelpTasksPlugin.HELP_GROUP, group)
        }
    }

    @Test fun `has runnable action`() {
        with(project.tasks.create("testPrintVersion", PrintVersionTask::class.java)) {
            assertTrue(hasTaskActions())
            // the actual output is tested in a functional test 
            printVersion()
        }
    }
}
