/*
 * Copyright 2019 - Knolle the Creator Cat (creatorcat.de) and project contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License that can be found in the LICENSE.txt file.
 */

package de.creatorcat.genever.gradle.functest

import de.creatorcat.kotiya.matchers.common.contains
import de.creatorcat.kotiya.test.core.assertThat
import kotlin.test.Test

class ConfigureAndPrintVersion : GeneverFuncTestBase() {

    @Test fun `print generated version`() {
        writeBuildScript(
            """
            version = genever.standardVersion {
                base = [1, 2, 3]
            }.generate {
                releaseType = FINAL
            }
            """
        )
        val result = runAndAssertTask("printVersion")
        assertThat(
            result.output,
            contains(Regex("Task :printVersion\\s*projectDir: 1.2.3"))
        )
    }
}
