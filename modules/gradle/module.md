# Module de.creatorcat.genever.gradle

The GeneVer Gradle module contains the Gradle plugin 
and further useful types and functionality to support using GeneVer in Gradle scripts. 

# Package de.creatorcat.genever.gradle

Contains Gradle objects (plugin, tasks and extensions)
and also Gradle specific extensions of certain GeneVer types to simplify usage in Gradle scripts.  
